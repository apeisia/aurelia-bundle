<?php

namespace Apeisia\AureliaBundle\Controller;

use Apeisia\AureliaBundle\Service\ControllerService;
use FOS\RestBundle\Controller\Annotations\Get;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\ChoiceList\View\ChoiceView;
use Symfony\Component\Form\FormView;
use Symfony\Component\HttpFoundation\Request;

class FormController extends AbstractController
{
    private $controllerService;

    public function __construct(ControllerService $controllerService)
    {
        $this->controllerService = $controllerService;
    }

    /**
     * @Get("/api/form/@{bundle}.{controller}.{action}", requirements={"_format"="json"}, name="form_select")
     * @param $bundle
     * @param $controller
     * @param $action
     *
     * @return array
     */
    #[Get('/api/form/@{bundle}.{controller}.{action}', requirements: ['_format' => 'json'], name: 'form_select')]
    public function formAction(Request $request, $bundle, $controller, $action)
    {
        $vars = $this->controllerService->getVariablesFromController($bundle, $controller, $action, $request);
        /** @var \Symfony\Component\Form\Form $form */
        $out = [];
        // look for forms
        foreach ($vars as $var) {
            if ($var instanceof FormView) {
                // loop through widgets
                $this->handleWidget($var, $out);
            }
        }

        return $out;
    }

    /**
     * @param FormView $widget
     * @param array &$out
     */
    private function handleWidget(FormView $view, array &$out)
    {
        foreach ($view->children as $widget) {
            if (isset($widget->vars['aureliatype']) && $widget->vars['aureliatype']) {
                // found choice. read choices
                $list = [];
                /** @var ChoiceView $choice */
                foreach ($widget->vars['choices'] as $choice) {
                    $value = $choice->value;
                    if (is_numeric($value))
                        $value = intval($value);
                    $item = [
                        'label' => $choice->label,
                        'value' => $value,
                    ];

                    if ($choice->attr && array_key_exists('additional_data', $choice->attr)) {
                        $item['additionalData'] = $choice->attr['additional_data'];
                    }

                    $list[] = $item;
                }
                $out[$widget->vars['id']] = $list;
            }

            if ($widget->children) {
                $this->handleWidget($widget, $out);
            }
        }
    }

}
