<?php

namespace Apeisia\AureliaBundle\FormType;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AureliaChoiceType extends ChoiceType
{

    public function buildView(FormView $view, FormInterface $form, array $options): void
    {
        parent::buildView($view, $form, $options);
        $view->vars['aureliatype'] = true;
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        parent::buildForm($builder, $options);

        $builder->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) {
            $parentChoice = $event->getData();
            $subChoices = $this->getValidChoicesFor($parentChoice);

            $event->getForm()->add('sub_choice', 'choice', [
                'label'   => 'Sub Choice',
                'choices' => $subChoices,
            ]);
        });
    }

}
