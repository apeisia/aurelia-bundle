<?php

namespace Apeisia\AureliaBundle\FormType;

use Doctrine\Persistence\ObjectManager;
use Symfony\Bridge\Doctrine\Form\ChoiceList\EntityLoaderInterface;
use Symfony\Bridge\Doctrine\Form\ChoiceList\ORMQueryBuilderLoader;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\ChoiceList\View\ChoiceView;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AureliaEntityType extends EntityType
{
    private static $_skipDatabaseQuery = false;

    public static function skipDatabaseQuery($skip = true)
    {
        self::$_skipDatabaseQuery = $skip;
    }

    public function buildView(FormView $view, FormInterface $form, array $options): void
    {
        parent::buildView($view, $form, $options);
        $view->vars['aureliatype'] = true;

        if ($options['choice_attr']) {
            $view->vars['choice_attr'] = $options['choice_attr'];
        }

        if ($options['additional_data']) {
            /** @var ChoiceView $choice */
            foreach ($view->vars['choices'] as $choice) {
                $var                             = $options['additional_data']($choice->data);
                $choice->attr['additional_data'] = $var;
            }
        }
        if ($options['filter']) {
            $choices = [];
            foreach ($view->vars['choices'] as $choice) {
                if($options['filter']($choice->data)) {
                    $choices[] = $choice;
                }
            }
            $view->vars['choices'] = $choices;
        }
        if ($options['postProcess']) {
            $view->vars['choices'] = $options['postProcess']($view->vars['choices']);
        }
    }

    public function getLoader(ObjectManager $manager, object $queryBuilder, string $class): ORMQueryBuilderLoader
    {
        if (self::$_skipDatabaseQuery) {
            return new EmptyArrayEntityLoader();
        }

        return parent::getLoader($manager, $queryBuilder, $class);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        parent::configureOptions($resolver);

        $resolver->setDefault('additional_data', null);
        $resolver->setAllowedTypes('additional_data', ['null', 'callable']);
        $resolver->setDefault('filter', null);
        $resolver->setAllowedTypes('filter', ['null', 'callable']);
        $resolver->setDefault('postProcess', null);
        $resolver->setAllowedTypes('postProcess', ['null', 'callable']);
    }

    public function getQueryBuilderPartsForCachingHash($queryBuilder): ?array
    {
        // disable caching of form objects completely, as this would otherwise reuse FormView objects
        // if the query builders SQL are the same.
        return null;
    }
}

class EmptyArrayEntityLoader implements EntityLoaderInterface {
    /**
     * Returns an array of entities that are valid choices in the corresponding choice list.
     *
     * @return array The entities
     */
    public function getEntities(): array
    {
        return [];
    }

    /**
     * Returns an array of entities matching the given identifiers.
     *
     * @param string $identifier The identifier field of the object. This method
     *                           is not applicable for fields with multiple
     *                           identifiers.
     * @param array  $values     The values of the identifiers
     *
     * @return array The entities
     */
    public function getEntitiesByIds(string $identifier, array $values): array
    {
        return [];
    }
}
