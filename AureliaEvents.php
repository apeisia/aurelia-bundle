<?php
namespace Apeisia\AureliaBundle;

use Apeisia\AureliaBundle\Event\AppConfigEvent;
use Apeisia\AureliaBundle\Event\ConfigureRouterEvent;

class AureliaEvents
{
    const APP_CONFIG = AppConfigEvent::class;
    const ROUTER_CONFIG = ConfigureRouterEvent::class;
}
