<?php
namespace Apeisia\AureliaBundle\Tests\Twig;

use Apeisia\AureliaBundle\Twig\AureliaFormExtension;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Form\FormView;

class AureliaFormExtensionTest extends TestCase
{
    public function testGetFormElementName()
    {
        $root   = new FormView();
        $foo    = new FormView($root);
        $foobar = new FormView($foo);

        $root->vars['name']   = 'root';
        $foo->vars['name']    = 'foo';
        $foobar->vars['name'] = 'foobar';

        $ext = new AureliaFormExtension();

        $this->assertEquals('root', $ext->getFormElementName($root));
        $this->assertEquals('foo', $ext->getFormElementName($foo));
        $this->assertEquals('foo.foobar', $ext->getFormElementName($foobar));
    }
}
