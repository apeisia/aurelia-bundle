import env from "environment";

/**
 * Log to log channel, including a timestamp.
 */
export function log() {
    console.log.apply(console, formatMessageToLog(arguments));
}

/**
 * Log to debug channel, including a timestamp, but only if the environment's debug flag is set to true
 */
export function debug(...args) {
    if (!env.debug) return;

    console.debug.apply(console, formatMessageToLog(arguments));
}

function formatMessageToLog(what) {
    return ['[' + formatTime() + ']'].concat(Array.prototype.slice.call(what));
}

function formatTime(date?) {
    if (!date) date = new Date();

    return date.toLocaleTimeString();
}
