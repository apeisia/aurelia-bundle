import {HttpClient, HttpResponseMessage} from "aurelia-http-client"
import {autoinject} from "aurelia-framework"

@autoinject
export class SimpleUpload {

    uploadInput
    uploadStatus
    url
    fieldName
    uploadStartCb

    constructor(private http: HttpClient, httpClientMethod: string = 'post') {
        // when injecting SimpleUpload through DI httpClientMethod is an empty string
        httpClientMethod = httpClientMethod == "" ? "post" : httpClientMethod

        this.uploadInput = document.createElement("input")
        this.uploadInput.setAttribute("type", "file")
        this.uploadInput.addEventListener("change", ev => {
            if (!this.uploadInput.files || !this.uploadInput.files.length) {
                // no files selected
                return
            }

            // prepare data and update view
            const fileName = this.uploadInput.files[0].name
            const fd = new FormData()
            fd.append(this.fieldName, this.uploadInput.files[0], fileName)

            this.uploadStatus = {
                name: fileName,
                running: true,
                error: false,
                progress: 0,
            }
            let promiseResolve, promiseReject
            let promise = new Promise((resolve, reject) => {
                promiseResolve = resolve
                promiseReject = reject
            })
            this.uploadStartCb(promise)

            // @ts-ignore
            this.http[httpClientMethod](this.url, fd, undefined, event => {
                this.uploadStatus.progress = event.loaded / event.total * 100
            }).then(response => {
                this.uploadStatus.running = false
                this.uploadStatus.progress = 0
                promiseResolve(response)
            }).catch(response => {
                this.uploadStatus.running = false
                this.uploadStatus.progress = 0
                this.uploadStatus.error = true
                promiseReject(response)
            })
        })
    }

    upload(url, uploadStartCb = null, accept = '*.*', fieldName = 'file') {
        this.url = url
        this.fieldName = fieldName
        this.uploadStartCb = uploadStartCb
        this.uploadInput.setAttribute("accept", accept)
        // clear inputs file list (https://stackoverflow.com/questions/3144419/how-do-i-remove-a-file-from-the-filelist)
        this.uploadInput.value = ""
        this.uploadInput.click()
    }

    asyncUpload(url, accept = '*.*', fieldName = 'file'): Promise<HttpResponseMessage> {
        return new Promise((resolve, reject) => {
            this.upload(url, (p) => {
                p.then(resolve).catch(reject)
            }, accept, fieldName)
        })
    }

}
