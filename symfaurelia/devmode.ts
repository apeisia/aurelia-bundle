import {autoinject} from "aurelia-framework";
import {DialogService} from "aurelia-dialog";
import {HttpClient} from "aurelia-http-client";
import {YesNoDialog} from "symfaurelia/dialogs/yes-no";
import {Toaster} from "symfaurelia/toaster";
import {PLATFORM} from "aurelia-pal";

@autoinject()
export default class {
    constructor(private http: HttpClient, private dialogService: DialogService, private toaster: Toaster) {

    }

    async tellUser(message): Promise<void> {
        return new Promise((resolve, reject) => {
            this.dialogService.open({
                viewModel: YesNoDialog,
                view: PLATFORM.moduleName("symfaurelia/overlays/devmode.html"),
                model: {
                    message: message
                }
            }).whenClosed(response => {
                if (response.wasCancelled) {
                    reject();
                } else {
                    resolve(null);
                }
            });
        });
    }

    toast(message) {
        this.toaster.add('success', 'Entwicklermodus Aktion erfolgreich', message);
    }

    async delete(message, url) {
        await this.tellUser(message);
        const r = await this.http.delete("devmode/" + url);
        this.toast(message);
        return r;
    }

    async post(message, url, data = {}) {
        await this.tellUser(message);
        const r = await this.http.post("devmode/" + url, data);
        this.toast(message);
        return r;
    }
}
