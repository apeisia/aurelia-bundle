export * from "./app-base"
export * from "./modules"

import {FrameworkConfiguration} from 'aurelia-framework'
import {PLATFORM} from 'aurelia-pal'

export function configure(config: FrameworkConfiguration) {
    config.globalResources([
        PLATFORM.moduleName('./attributes/classes'),
        PLATFORM.moduleName('./attributes/spinner'),
        PLATFORM.moduleName('./attributes/hash-tab'),
        PLATFORM.moduleName('./value-converters/datetime'),
        PLATFORM.moduleName('./value-converters/objects'),
        PLATFORM.moduleName('./elements/dropdown/dropdown'),
    ])
}
