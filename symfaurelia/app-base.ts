import {RouteRecognizer} from "aurelia-route-recognizer"
import {AbstractApp} from "./abstract-app"
import {PLATFORM} from "aurelia-pal"

export class AppBase extends AbstractApp {
    configureRouter(config, router) {
        config.title = this.TITLE
        config.options.pushState = true
        config.mapUnknownRoutes(PLATFORM.moduleName('error/404'))

        router.transformTitle = title => {
            let params = {}
            this.eventAggregator.publish('router:transformTitle', params)
            return this.i18n.tr(title, params)
        }

        let baseUrl = this.ROUTE_APP_PREFIX
        if (this.config && this.config.routes) {
            // this gets called always, even if an error occurred while loading the config. make this optional
            // to prevent complains from the aurelia router...
            config.map(prefixRoutes(baseUrl, this.config.routes))
        }
        config.fallbackRoute(baseUrl)

        this.router = router

        // @TsIgnore
        this.observerLocator.getObserver(router, "currentInstruction").subscribe((newValue, oldValue) => {
            if (newValue !== null && oldValue === null) {
                this.eventAggregator.publish('router:configured')
            }
        })
    }

    async reloadAppConfig() {
        try {
            let data = await this.http.get("_app_config.json")
            this.config = data.content

            let headers = data.headers.headers
            if (headers['x-frontend-rev']) {
                if (!this.frontendRev) { // only update frontend rev if we don't have one. first value ever counts.
                    this.frontendRev = headers['x-frontend-rev'].value
                }
            }

            if (this.router) {
                // router already configured. add new routes

                // this.router.reset();
                this.router.routes = []
                this.router._recognizer = new RouteRecognizer()
                this.router.configure(config => {
                    this.configureRouter(config, this.router)
                })

                //
                // for (let route of persistentRoutes) {
                //     this.router.addRoute(route);
                // }
                //
                // for(let route of prefixRoutes(this.ROUTE_APP_PREFIX, this.config.routes)) {
                //     this.router.addRoute(route);
                // }

                this.router.refreshNavigation()
                await this.router.ensureConfigured()
            }

            this.eventAggregator.publish('appconfig:changed', this.config)
            return true
        } catch (e) {
            if (e.statusCode == 401) {
                let err
                if (typeof e.content === 'object')
                    if (e.content.message) {
                        err = e.content.message
                    } else {
                        err = e.content.error.exception[0].message
                    }
                else
                    err = e.content

                if (err.toLowerCase().includes('authentication') && err.toLowerCase().includes('required'))
                    return false

                if (err == 'Account is disabled')
                    err = 'Der Account ist gesperrt'

                if (typeof err === 'string')
                    alert(err)
            }
            return false
        }
    }

    async activate() {
        if (window.location.pathname != '/login') {
            await this.reloadAppConfig()
        }
    }

    requestReload() {
        this.dialog.confirm("Update eingespielt", "Seit Ihrer letzten Aktion wurde ein Softwareupdate eingespielt."
            + " Wollen Sie die Seite neu laden, um das Update zu aktivieren?").then(() => {
            return location.reload(true)
        })
    }

    httpResponse(message) {
    }

    httpResponseError(error) {
    }
}

function prefixRoutes(prefix, routes) {
    for (let route of routes) {
        route.route = prefix + route.route
        // this is only required to reload the currently active route, enable when needed.
        // route.activationStrategy = activationStrategy.invokeLifecycle;
    }

    return routes
}

export function getAccountFromUrl(url) {
    let chident_prefix = "/use/"
    if (url.indexOf(chident_prefix) === 0) {
        url = url.substr(chident_prefix.length)
        let end = url.indexOf('/')
        if (end >= 0) {
            return url.substr(0, end)
        }

        return url
    }
    return null
}

