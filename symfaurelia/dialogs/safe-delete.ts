import {autoinject} from "aurelia-framework"
import {DialogController} from "aurelia-dialog"
import {BaseDialog} from "./base"

@autoinject()
export class SafeDeleteDialog extends BaseDialog {
    value;
    requiredValue;

    constructor(controller: DialogController) {
        super(controller);
    }

    activate(config) {
        super.activate(config);
        this.value = config.value || "";
        this.requiredValue = config.requiredValue;
    }

    submit() {
        if (this.value != this.requiredValue) {
            return;
        }

        this.controller.ok(this.value);
    }
}
