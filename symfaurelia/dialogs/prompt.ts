import {BaseDialog} from "./base"
import {DialogButton} from "../dialog";

// @inject(DialogController)
export class SuccessDialog extends BaseDialog {
    text;
    inputType = "textarea";

    activate(params) {
        super.activate(params);

        this.inputType = params.options.inputType;
        this.text = params.options.text || "";
    }

    buttonClicked(button: DialogButton) {
        if (button.handler) {
            button.handler(this.controller, this.text);
        }
    }
}
