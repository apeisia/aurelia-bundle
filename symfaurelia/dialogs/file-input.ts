import {autoinject} from "aurelia-framework"
import {DialogController} from "aurelia-dialog"
import {BaseDialog} from "./base"

@autoinject()
export class TextInputDialog extends BaseDialog {
    cancellable = false;
    accept = "";
    multiple = false;
    fileInput: HTMLInputElement;

    constructor(controller: DialogController) {
        super(controller);
    }

    activate(config) {
        super.activate(config);

        if (config.options.cancellable) {
            this.cancellable = config.options.cancellable;
        }

        if (config.options.accept) {
            this.accept = config.options.accept;
        }

        if (config.options.multiple) {
            this.multiple = config.options.multiple;
        }
    }

    save() {
        this.controller.ok(this.fileInput.files);
    }
}
