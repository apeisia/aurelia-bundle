import {BaseDialog} from "./base"
import {DialogButton} from "../dialog";

// @inject(DialogController)
export class SuccessDialog extends BaseDialog {
    buttonClicked(button: DialogButton) {
        if (button.handler) {
            button.handler(this.controller);
        }
    }
}
