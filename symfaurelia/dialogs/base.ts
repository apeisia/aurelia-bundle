import {autoinject} from "aurelia-framework"
import {DialogController} from "aurelia-dialog"

@autoinject()
export class BaseDialog {
    controller: DialogController;
    title: string;
    message: string;
    icon: string|false;
    options;

    constructor(controller: DialogController) {
        this.controller = controller;
        this.title = "";
        this.message = "";
        this.icon = false;
    }

    activate(config) {
        if (config) {
            this.title = config.title || "";
            this.message = config.message || "";
            this.icon = config.icon || false;
            this.options = config.options || {};
            console.log(this);

            // modal is used to remove the close button
            if (config.modal === undefined) {
                config.modal = false;
            }

            if (this.options.icon) {
                this.icon = this.options.icon;
            }

            if (this.options.modal) {
                config.modal = this.options.modal;
            }

            this.controller.settings.lock = config.modal;
        }
    }
}
