import {observable} from "aurelia-framework"
import {Base} from "./base"
import {deepCopy} from "symfaurelia/util"

export abstract class List extends Base {
    list = []
    orderField = null
    orderDirection = null
    searchTimeout = null
    @observable()
    searchValue = ""
    @observable()
    itemsPerPage = 20
    filter: any = {}
    additionalListState: any = {}

    totalItems
    pageCount
    page
    pagination
    /**
     * are we activated yet?
     * itemsPerPageChanged() is called before we are activated, so ignore it until we are...
     * @type {boolean}
     */
    activated = false

    abstract fields: {
        name: string
        sortable?: boolean,
        sortfield?: string,
        title: string,
        type: 'string' | 'integer' | 'float' | 'currency' | 'datetime' | 'datetimeshort' | 'date' | 'bool' | 'html' | string,
        getter?: (any) => string,
        thClass?: () => string,
        tdClass?: (any) => string,
    }[]

    filterFields:
        {
            name: string,
            field: string,
            items: { name: string, value: string }[] | { name: string, items: { name: string, value: string }[] }[]
        }[]


    /**
     * The number of pages that should be shown in the pagination BEFORE and AFTER the current page.
     * @type {number}
     */
    pageRange = 2


    // BC aliases (read-only!)
    // todo: this should be removed and MUST be changed in all modules once we use the symfaurelia-commons in vwx.
    _refreshPagination = this.refreshPagination
    _getListPath = this.getListPath
    _listLoaded = this.listLoaded
    _onEntityDeleted = this.onEntityDeleted
    _getDefaultOrderField = this.getDefaultOrderField
    _getDefaultOrderDirection = this.getDefaultOrderDirection

    activate(params = null) {
        const listState = JSON.parse(sessionStorage.getItem('list-state-' + this.$config.list.id()) || '{}')

        if (listState) {
            if (listState.searchValue) {
                this.searchValue = listState.searchValue
            }

            if (listState.itemsPerPage) {
                this.itemsPerPage = listState.itemsPerPage
            }

            if (listState.orderField) {
                this.orderField = listState.orderField
            }

            if (listState.orderDirection) {
                this.orderDirection = listState.orderDirection
            }

            if (listState.page) {
                this.page = listState.page
            }

            if (listState.filter && Object.keys(this.filter).length === 0) {
                this.filter = listState.filter
            }

            if (listState.additionalListState) {
                this.additionalListState = listState.additionalListState
            }
        }

        if (!this.searchValue) this.searchValue = ''
        this.activated = true
        super.activate(params)

        const self = (this as any)
        if (self.inlineView) {
            console.warn('[list] INLINE VIEW COMPAT MODE')
            this.$config.inline.mode = 'in-table'
            this.$config.list.onClick = (item, field) => {
                let iv = self.inlineView
                if (self.inlineViewForColumn?.[field]) {
                    iv = self.inlineViewForColumn[field]
                }
                let out = deepCopy(iv)
                if (typeof out == 'string') {
                    out = {
                        view: out,
                    }
                }
                if (iv.model && typeof iv.model === 'function') {
                    out.model = iv.model(item)
                }
                if (!out.model) {

                    out.model = {
                        id: typeof item == 'object' ? item.id : undefined,
                        openRow: {
                            item,
                        },
                    }
                }
                console.warn(item, out.model)
                return out
            }
            const resolveCompat = x => typeof x === 'function' ? x() : x
            this.$config.inline.edit = resolveCompat(self.inlineView)
            this.$config.inline.show = resolveCompat(self.inlineView)
        }
        for (let what of ['edit', 'new', 'show']) {
            if (this.$config.useDialogs[what]) {
                console.warn('[list] DIALOGS COMPAT MODE')
                this.$config.inline[what] = item => {
                    return {
                        view: this.$config.useDialogs[what](item),
                        model: item,
                        mode: 'dialog',
                    }
                }
            }
        }
        return this.refresh()
    }

    refresh(background = false) {
        if (!this.orderField) {
            this.orderField = this.getDefaultOrderField()
        }
        if (!this.orderDirection) {
            this.orderDirection = this.getDefaultOrderDirection()
        }

        sessionStorage.setItem('list-state-' + this.$config.list.id(), JSON.stringify({
            'searchValue': this.searchValue,
            'itemsPerPage': this.itemsPerPage,
            'orderField': this.orderField,
            'orderDirection': this.orderDirection,
            'page': this.page,
            'filter': this.filter,
            'additionalListState': this.additionalListState,
        }))

        let loadFn
        if (background) {
            loadFn = (<any>this.http).getBg.bind(this.http)
        } else {
            loadFn = this.http.get.bind(this.http)
        }

        return loadFn(this.getListPath(), this.getListParams()).then(data => {
            this.list = data.content.items || data.content.list
            this.totalItems = data.content.totalCount || data.content.totalItems
            this.pageCount = data.content.pageCount
            this.page = parseInt(data.content.currentPageNumber || data.content.page) // todo: do this in php!
            this.refreshPagination()
            this.listLoaded(data)

            if (this.page > this.pageCount && this.pageCount > 0) {
                this.page = this.pageCount
                this.refresh()
            }

            return null
        })
    }

    /**
     * @deprecated use refresh() instead. Note that the foreground parameter became background and thus is inverted.
     */
    refreshList(foreground = true) {
        return this.refresh(!foreground)
    }

    getListParams(): any {
        let orderBy = this.getField(this.orderField, "sortfield")
        return {
            search: this.searchValue,
            sort: orderBy,
            direction: this.orderDirection,
            entriesperpage: this.itemsPerPage,
            page: this.page,
            filter: this.filter,
        }
    }

    searchValueChanged(newVal, oldVal) {
        if (!this.activated) return // dont care if search value changed if we are not activated yet
        clearTimeout(this.searchTimeout)
        this.searchTimeout = setTimeout(() => {
            this.refresh()
        }, 1000)
    }

    search() {
        this.page = 1
        clearTimeout(this.searchTimeout)
        this.refresh()
    }

    goPage(num) {
        if (num === this.page || num < 1 || num > this.pageCount) {
            return false
        }

        this.page = num
        this.refresh()

        return false
    }

    itemsPerPageChanged(nv, ov) {
        if (!this.activated) return // dont care if items per page changed if we are not activated yet
        if (this.itemsPerPage * this.page > this.totalItems) {
            // the current page would exceed the maximum number of pages with the new itemsPerPage, adjust the page
            // todo do this in php too...
            this.page = Math.ceil(this.totalItems / this.itemsPerPage)
        }

        this.refresh()
    }

    refreshPagination() {
        let pages = []
        let start = this.page - this.pageRange
        let end = this.page + this.pageRange

        if (start <= 0) {
            // less than <pageRange> pages before the current page, extend the number of pages after the current page
            end += -start + 1
            start = 1
        }

        if (end >= this.pageCount) {
            // more pages after page than maximum page number, cut off the rest and append to the beginning
            start -= end - this.pageCount
            end = this.pageCount
        }

        // check the result and adjust it if something is out of bounds
        if (start <= 0) {
            start = 1
        }

        if (end >= this.pageCount) {
            end = this.pageCount
        }

        // build the pagination array that we can iterate in the template
        for (let i = start; i <= end; ++i) {
            pages.push(i)
        }

        this.pagination = pages
    }

    /**
     * Return the field with the given fieldName or null if it does not exist.
     * If a property is given, return the property from that field or null if the field or the property does not exist.
     *
     * @param fieldName
     * @param property
     * @returns {*}
     */
    getField(fieldName, property = null) {
        if (!this.fields) return null
        let field = null
        for (let i of this.fields) {
            if (i.name === fieldName) {
                field = i
            }
        }

        if (!property) {
            return field
        }

        if (property && !field) {
            return null
        }

        return field[property]
    }

    isSortable(fieldName) {
        return this.getField(fieldName, "sortable") || false
    }

    toggleOrderDirection() {
        if (this.orderDirection === "asc") {
            this.orderDirection = "desc"
        } else {
            this.orderDirection = "asc"
        }
    }

    orderBy(field) {
        if (!this.isSortable(field)) return

        if (this.orderField === field) {
            this.toggleOrderDirection()
        } else {
            this.orderField = field
            this.orderDirection = "asc"
        }

        return this.refresh()
    }

    readField(item, field) {
        if (field.getter) {
            return field.getter(item)
        }

        let propertyPath = field.name.split(".")
        let value = item

        for (let segment of propertyPath) {
            value = value[segment]
        }

        return value
    }

    getListPath() {
        return this.$config.apiBase() + '/'
    }

    listLoaded(response) {
    }

    onEntityDeleted() {
        this.refreshList()
    }

    getDefaultOrderField() {
        if (!this.fields) return null
        if (this.fields.length > 0) {
            return this.fields[0].name
        }
    }

    getDefaultOrderDirection() {
        return 'asc'
    }

    itemClicked(item, field): any {
        return this.handleEntityAction(item, null, 'field', field.name)
    }

    itemClickable(item, field) {
        return true
    }
}
