import {Base} from "./base"

export abstract class Show extends Base {
    item

    // BC aliases (read-only!)
    // todo: this should be removed and MUST be changed in all modules once we use the symfaurelia-commons in vwx.
    _onEntityLoaded = this.onEntityLoaded
    _getCloneConfirmation = this.getCloneConfirmation

    activate(params): Promise<any | void> {
        super.activate(params)
        if (params.id) {
            return this.loadEntity(params.id)
        } else if (params.copy) {
            return this.loadEntity(params.copy).then(() => {
                this.item.id = undefined
            })
        } else {
            return new Promise<void>((resolve, reject) => {
                this.createEmptyEntity()
                resolve(null)
            })
        }
    }

    attached() {
        this.eventAggregator.publish("entity:attached", this)
    }

    refresh() {
        if (!this.item) {
            return
        }

        return this.loadEntity(this.item.id)
    }

    createEmptyEntity() {
    }

    /**
     * Return true if we have an unsaved entity.
     * @returns boolean
     */
    isNew() {
        return !this.item || !this.item.id
    }

    /**
     * Load the entity from the backend, see `$config.entityPath` and `onEntityLoaded()`.
     * @param id
     * @returns {Promise}
     */
    loadEntity(id) {
        return new Promise((resolve, reject) => {
            this.http.get(this.$config.entityPath(id)).then(data => {
                this.item = data.content
                this.onEntityLoaded()
                resolve(this.item)
                return data
            }).catch(error => {
                reject(error)
                return error
            })
        })
    }

    deleteEntity(item) {
        if (item === undefined) {
            item = this.item
        }

        return super.deleteEntity(item)
    }

    /**
     * Optional life-cycle hook for when the entity is loaded.
     * @protected
     */
    onEntityLoaded() {
    }

    /**
     * Get the dialog message that will be shown for confirmation when cloning an entity.
     * @param item
     * @returns {string}
     * @protected
     */
    getCloneConfirmation(item) {
        return this.i18n.tr("crud:clone.confirmation", {
            type: this.getReadableName(),
            name: this.getReadableEntity(item),
        })
    }

    /**
     * Return true, preferable from your modules base, if you want to allow cloning.
     *
     * @returns {boolean}
     */
    allowCloning() {
        return false
    }

    clone() {
        if (!this.allowCloning()) {
            return
        }

        this.dialog.confirm(this.i18n.tr("crud:clone.confirmation", {type: this.getReadableName()}), this.getCloneConfirmation(this.item)).then(() => {
            this.http.post(this.$config.entityPath() + "?copyFrom=" + this.item.id, undefined)
                .then(data => data.content)
                .then(newItem => {
                    this.toaster.add("success",
                        this.i18n.tr("crud:clone.success.title", {type: this.getReadableName()}),
                        this.i18n.tr("crud:clone.success.message", {
                            type: this.getReadableName(),
                            name: this.getReadableEntity(this.item),
                        }),
                    )

                    this.router.navigateToRoute(this.getRoutePrefix() + ".show", this.$config.routeParams(newItem))
                    return newItem
                }).catch(response => {
                if (response.statusCode === 412 /*Precondition Failed*/) {
                    let violations = response.content
                    let msg = ''
                    for (let v of violations) {
                        msg += v.message + "\n"
                    }
                    this.toaster.add("error",
                        this.i18n.tr("crud:clone.error.title", {type: this.getReadableName()}),
                        msg,
                    )
                }
                return response
            })
        })
    }
}
