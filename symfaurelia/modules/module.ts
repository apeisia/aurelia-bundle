import {Container} from "aurelia-dependency-injection";

export function module(moduleBase) {
    let moduleInstance = Container.instance.get(moduleBase);
    let modulePrototype = moduleBase.prototype;

    return (target) => {
        for (let key in modulePrototype) {
            if (modulePrototype.hasOwnProperty(key)) {
                // write all functions from the module base class to the target class prototype
                target.prototype[key] = modulePrototype[key];
            }
        }

        if (moduleInstance) {
            for (let key in moduleInstance) {
                if (moduleInstance.hasOwnProperty(key) && typeof(moduleInstance[key]) !== "function") {
                    // write all properties from the module base class to the target class prototype
                    target.prototype[key] = moduleInstance[key];
                }
            }
        }
    };
}

export function moduleConfig(moduleConfig) {
    return target => {
        target.$config = moduleConfig;
    };
}


interface ModuleConfig {
    apiPrefix?: string;
    routePrefix?: string;

    readableName?: string | {
        singular: string,
        plural: string,
    };

    safeDeleteDialog?: boolean | {
        active: boolean,
        confirmationString?: string | ((object) => string);
    };
}

export function resolveConfig(config: ModuleConfig): ModuleConfig {
    config.apiPrefix = config.apiPrefix || null;
    config.routePrefix = config.routePrefix || null;
    config.readableName = config.readableName || null;
    config.safeDeleteDialog = config.safeDeleteDialog || false;


    if (config.safeDeleteDialog === false) {
        config.safeDeleteDialog = {
            active: false,
        };
    }

    if (config.safeDeleteDialog === true) {
        config.safeDeleteDialog = {
            active: true,
        };
    }

    if (config.safeDeleteDialog && !config.safeDeleteDialog.confirmationString) {
        config.safeDeleteDialog.confirmationString = item => item.name;
    }

    if (typeof config.readableName !== "object") {
        config.readableName = {
            singular: config.readableName,
            plural: config.readableName,
        };
    }

    return config;
}
