import {HttpClient} from "aurelia-http-client"
import {Router} from "aurelia-router"
import {Toaster} from "../toaster"
import {Dialog} from "../dialog"
import {Container} from "aurelia-dependency-injection"
//@ts-ignore
import {App} from "app"
import {I18N} from "aurelia-i18n"
import {autoinject} from "aurelia-framework"
import {EventAggregator} from "aurelia-event-aggregator"
import {SafeDeleteDialog} from "../dialogs/safe-delete"
import {DialogCancellableOpenResult, DialogController, DialogOpenPromise} from "aurelia-dialog"
import {CloseReason, InlineTable, WhenClosedPromise} from "symfaurelia/modules/inline-table"

export interface CrudViewModelInterface {
    view?: string,
    model?: any,
    mode?: null | 'dialog' | 'in-table' | 'router',
}

type ViewModelName = string

export interface ModuleConfiguration {
    safeDeleteDialog: {
        /** The value that must be entered to delete the entity */
        confirmationProperty: void | ((any) => string),

        /** Enable or disable the safe delete dialog, will be a yes / no dialog if set to false */
        active: boolean,

        /** The prompt that is shown to the user, gets the entity as only parameter */
        prompt: (object) => string,
    };

    list: {
        /** Used to globally identify this list when storing the list state */
        id: () => string,

        /** Defaults to "show", null means no action */
        onClick: 'edit' | 'show' | null | ((entity, field) => (ViewModelName | CrudViewModelInterface | null)),

        copyButton: boolean
    };

    /**
     * Should specific things be opened as a dialog instead of navigating to the page?
     *
     * @deprecated use inline instead
     */
    useDialogs: {
        edit: false | (() => string),
        show: false | (() => string), // not supported yet
        list: false | (() => string) // not supported yet
    }

    inline: {
        mode: null | 'dialog' | 'in-table',
        edit: null | ((item) => ViewModelName | CrudViewModelInterface),
        show: null | ((item) => ViewModelName | CrudViewModelInterface),
        new: 'edit' | ((item) => ViewModelName | CrudViewModelInterface),
    }

    /** Gets prepended to all api urls */
    apiBase: () => string;

    /** Gets the parameters that should be passed to all routes */
    routeParams: (entity) => object;

    /** Get the REST url for the given entity. By default this is automatically built via the apiBase configuration */
    entityPath: (entity?) => string;

    /** What happens after an entity was saved? null means no action */
    afterSaveAction: null | "auto" | "close" | "close-dialog" | "navigate-list" | "navigate-show" | ((any) => any);

    /** What happens after an entity was deleted? null means no action */
    afterDeleteAction: null | "auto" | "close" | "navigate-list" | ((any) => any);
}

@autoinject()
export abstract class Base {
    public $config: ModuleConfiguration = {
        useDialogs: {
            edit: false,
            show: false,
            list: false,
        },
        inline: {
            mode: null,
            edit: null,
            show: null,
            new: 'edit',
        },
        safeDeleteDialog: {
            confirmationProperty: null,
            active: false,
            prompt: item => this.i18n.tr("crud:delete.safe_delete_prompt", {
                value: this.getValueOrCall(this.$config.safeDeleteDialog.confirmationProperty, item),
                interpolation: {escapeValue: false},
            }),
        },
        list: {
            id: () => location.pathname,
            onClick: "show",
            copyButton: false,
        },
        apiBase: () => this.getApiBase(),
        routeParams: (entity) => this.getRouteParams(entity),
        entityPath: (entity) => this.getEntityPath(entity),
        afterSaveAction: "auto",
        afterDeleteAction: () => this.onEntityDeleted(),
    }

    readableName = ''
    params: any = {}

    transformTitleSubscriber
    navigationProcessingSubscriber
    localeChangedSubscriber

    // BC aliases (read-only!)
    // todo: this should be removed and MUST be changed in all modules once we use the symfaurelia-commons in vwx.
    /** @deprecated use $config.apiBase instead **/
    _getApiBase = this.getApiBase
    _getRoutePrefix = this.getRoutePrefix
    _getReadableName = this.getReadableName
    _getReadableEntity = this.getReadableEntity
    _getEntityPath = this.getEntityPath
    _localNavigate = this.localNavigate
    _getDeleteConfirmation = this.getDeleteConfirmation
    _onEntityDeleted = this.onEntityDeleted

    constructor(public http: HttpClient,
                public toaster: Toaster,
                public router: Router,
                public dialog: Dialog,
                public container: Container,
                public app: App,
                public i18n: I18N,
                public dialogController: DialogController,
                public eventAggregator: EventAggregator,
                public inlineTable: InlineTable,
    ) {
        this.init()
        this.configureModule(this.$config)
    }

    activate(params = undefined) {
        this.transformTitleSubscriber = this.app.eventAggregator.subscribe('router:transformTitle', this.transformTitle.bind(this))
        this.navigationProcessingSubscriber = this.app.eventAggregator.subscribe('router:navigation:processing', this.navigationProcessing.bind(this))
        this.localeChangedSubscriber = this.app.eventAggregator.subscribe('i18n:locale:changed', this.rebindReadableName.bind(this))

        if (params) {
            this.params = params
        }
    }

    navigationProcessing() {
        this.transformTitleSubscriber.dispose()
        this.navigationProcessingSubscriber.dispose()
        this.localeChangedSubscriber.dispose()
        this.inlineTable.dispose()
    }

    transformTitle(params) {
    }

    attached() {
        this.rebindReadableName()
    }

    rebindReadableName() {
        // the binding system detects property changes. change when required to avoid aurelia dirty checking
        this.readableName = this.getReadableName()
    }

    /**
     * Optional life-cycle hook that is called after the constructor.
     */
    protected init() {

    }

    /**
     * The base path of the REST resource, e.g. /user
     * @protected
     * @abstract
     * @deprecated Use $config.apiBase instead
     */
    getApiBase(): string {
        throw Error('you must implement $config.apiBase()')
    }

    /**
     * Get the common prefix for the resources routes, e.g. @Vwx.User
     * @protected
     */
    getRoutePrefix() {
        throw Error('you must implement getRoutePrefix()')
    }

    /**
     * @deprecated
     * @param item
     */
    getRouteParams(item = null) {
        if (item && item.id) {
            return {id: item.id}
        }
        return {}
    }

    /**
     * Human readable name for this entity type, e.g. "User"
     * @protected
     */
    getReadableName(): string {
        return ''
    }

    /**
     * Get the readable name for this entity. Should, for example, return the *name* of a user. By default
     * this returns the "name" property if it exist. Otherwise the "title" property and if that doesn't exist
     * either, the id prefixed by an "#". Overwrite as needed.
     * @param item
     * @returns {string}
     * @protected
     */
    getReadableEntity(item) {
        if (item.name) return item.name
        if (item.title) return item.title

        return '#' + item.id
    }

    /**
     * Get the resource url for the given item.
     * @param item
     * @returns {string}
     * @protected
     * @deprecated
     */
    getEntityPath(item?) {
        let url = this.$config.apiBase() + '/'

        if (item && item.id) {
            url += item.id
        } else if ("" + item === "" + parseInt(item, 10) || typeof item === "string") {
            // "item" is integer or string
            url += item
        }

        return url
    }

    /**
     * Navigate to a sub-route of this resource.
     * @param to
     * @param params
     * @protected
     */
    localNavigate(to, params?) {
        if (this.params[Symbol.for("inlineView")]) {
            throw new Error('do not use localNavigate() when in inline context')
        } else {
            this.router.navigateToRoute(this.getRoutePrefix() + '.' + to, params)
        }
    }

    /**
     * Shows a confirmation dialog to delete the given entity, or the current item if none is given, and delete it if
     * user confirmed the dialog, show a toast and redirect to the index page.
     * @param item
     */
    deleteEntity(item) {
        let callback = () => {
            this.http.delete(this.$config.entityPath(item)).then(() => {
                this.toaster.add("success", this.i18n.tr("crud:delete.success.title", {type: this.getReadableName()}),
                    this.i18n.tr("crud:delete.success.message", {
                        type: this.getReadableName(),
                        name: this.getReadableEntity(item),
                        interpolation: {escapeValue: false},
                    }))

                this.executeAfterAction(this.$config.afterDeleteAction, item, false, true)

                // deprecated
                this.eventAggregator.publish("entity-deleted", {entity: item})

                // use this instead
                this.eventAggregator.publish("entity:deleted", {entity: item, vm: this})

                return null
            })
            return null
        }

        if (this.$config.safeDeleteDialog && this.$config.safeDeleteDialog.active) {
            this.dialog.service.open({
                viewModel: SafeDeleteDialog,
                view: "symfaurelia/dialogs/safe-delete.html",
                model: {
                    title: this.i18n.tr("crud:delete.confirmation"),
                    message: this.getValueOrCall(this.$config.safeDeleteDialog.prompt, item),
                    requiredValue: this.getValueOrCall(this.$config.safeDeleteDialog.confirmationProperty, item),
                },
            }).whenClosed(response => {
                if (!response.wasCancelled) {
                    callback()
                }
            })
        } else {
            this.dialog.confirm(this.i18n.tr("crud:delete.confirmation"), this.getDeleteConfirmation(item)).then(callback)
        }
    }

    /**
     * Get the dialog message that will be shown for confirmation when deleting an entity.
     * @param item
     * @returns {string}
     * @protected
     */
    getDeleteConfirmation(item) {
        return this.i18n.tr("crud:delete.confirmation_detail", {
            type: this.getReadableName(),
            name: this.getReadableEntity(item),
        })
    }

    /**
     * Optional life-cycle hook for when the entity was deleted successfully. Navigates to the index route by default.
     * @protected
     * @deprecated Use $config.afterDeleteAction instead
     */
    onEntityDeleted() {
    }

    protected getValueOrCall(property, ...params) {
        if (typeof property === "function") {
            return property(...params)
        }

        return property
    }

    protected configureModule($config: ModuleConfiguration) {
        void ($config) // ignore not used warning
    }

    editEntity(entity = null): void | DialogOpenPromise<DialogCancellableOpenResult> | WhenClosedPromise<void> {
        return this.handleEntityAction(entity, entity && entity.id ? 'edit' : 'new', 'button')
    }

    showEntity(entity = null): void | DialogOpenPromise<DialogCancellableOpenResult> | WhenClosedPromise<void> {
        return this.handleEntityAction(entity, 'show', 'button')
    }

    protected handleEntityAction(entity, action, type: 'button' | 'field', fieldName = null): void | DialogOpenPromise<DialogCancellableOpenResult> | WhenClosedPromise<void> {
        const routeParams = this.$config.routeParams(entity)

        const inlineViewModel = this.resolveInlineConfigurationToCrudViewModel(entity, action, type, fieldName)
        if (inlineViewModel.mode === 'router') {
            this.router.navigateToRoute(this.getRoutePrefix() + '.' + (action ?? inlineViewModel.view), routeParams)
        } else {
            return this.executeInlineAction(inlineViewModel, action == 'new' ? 'new' : entity.id)
        }
    }

    protected executeInlineAction(inlineViewModel: CrudViewModelInterface, rowId = null): void | DialogOpenPromise<DialogCancellableOpenResult> | WhenClosedPromise<void> {
        if (!inlineViewModel.model) {
            inlineViewModel.model = {}
        }
        if (inlineViewModel.mode === 'dialog') {
            inlineViewModel.model[Symbol.for("inlineView")] = {
                mode: 'dialog',
                rowId,
            }

            const dialogPromise = this.dialog.service.open({
                viewModel: inlineViewModel.view,
                view: inlineViewModel.view + ".html",
                model: inlineViewModel.model,
                lock: false,
                overlayDismiss: false,
            })
            dialogPromise.whenClosed(result => {
                if (result.wasCancelled) {
                    return
                }

                if (typeof this["refresh"] === "function") {
                    this["refresh"]()
                }

                this.eventAggregator.publish('entity-changed', {entity: inlineViewModel.model})
            }).then()

            return dialogPromise
        } else if (inlineViewModel.mode === 'in-table') {
            return this.inlineTable.open(inlineViewModel.model, inlineViewModel.view, rowId).whenClosed((closeReason: CloseReason) => {
                if (closeReason.saved || closeReason.deleted) {
                    if (typeof this["refresh"] === "function") {
                        this["refresh"]()
                    }
                }
            })
        } else {
            throw new Error('Unknown inline mode "' + inlineViewModel.mode + '"')
        }
    }

    protected inlineClose(saved = false, deleted = false) {
        const mode = this.params[Symbol.for("inlineView")].mode
        if (mode == 'dialog') {
            this.dialogController.ok().then()
        } else if (mode == 'in-table') {
            this.params[Symbol.for("inlineView")].inlineTable.close({closedByView: true, saved, deleted})
        } else {
            throw new Error('Unknown inline mode "' + mode + '"')
        }
    }

    protected resolveInlineConfigurationToCrudViewModel(model, action, actionSource: 'button' | 'field', fieldName = null): CrudViewModelInterface {
        const resolveObjectOrFunction = (argument): CrudViewModelInterface => {
            if (typeof argument === 'function') {
                return argument(model, fieldName)
            } else {
                return argument
            }
        }

        if (!this.$config.inline && this.$config.useDialogs) {
            const convertAction = (action: false | (() => string)): null | ((item) => ViewModelName) => {
                if (action === false) {
                    return null
                }

                return action
            }

            this.$config.inline = {
                mode: "dialog",
                edit: convertAction(this.$config.useDialogs.edit),
                show: convertAction(this.$config.useDialogs.show),
                new: convertAction(this.$config.useDialogs.edit),
            }
        }

        let mode: string = this.$config.inline.mode

        if (!mode) {
            mode = 'router'
        }

        let result
        if (actionSource === 'field') {
            result = resolveObjectOrFunction(this.$config.list.onClick)
            if (['edit', 'show'].includes(result)) {
                // field click should be handled same as a button
                actionSource = 'button'
                action = result
            }
        }
        if (actionSource === 'button') {
            if (!['show', 'edit', 'new'].includes(action)) {
                throw Error('unsupported button type')
            }
            const cfg = this.$config.inline[action]
            if (cfg !== null) {
                result = resolveObjectOrFunction(cfg)
                if (['show', 'edit', 'new'].includes(result)) {
                    result = resolveObjectOrFunction(this.$config.inline[result])
                }
                // if (result === null) {
                //     throw Error('result after resolving $config.inline.* is null')
                // }
            }
        }

        if (typeof result === 'string') {
            result = {
                view: result,
            }
        }

        if (mode != 'router' && !result.view) {
            throw Error('View is not set.')
        }

        return {
            model,
            mode,
            ...result,
        }
    }

    protected executeAfterAction(afterAction, item, saved = false, deleted = false) {

        if (afterAction === "auto") {
            if (this.params[Symbol.for("inlineView")]) {
                afterAction = "close"
            } else {
                afterAction = "navigate-show"
            }
        }

        if (afterAction === "close-dialog") {
            console.warn('please change close-dialog to close')
            afterAction = 'close'
        }
        if (afterAction === "close") {
            this.inlineClose(saved, deleted)
        } else if (afterAction === "navigate-show") {
            this.localNavigate('show', this.$config.routeParams(item))
        } else if (afterAction === "navigate-list") {
            this.localNavigate('index', this.$config.routeParams(item))
        } else if (typeof afterAction === "function") {
            afterAction(item)
        }
    }
}
