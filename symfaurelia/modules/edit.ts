import {Show} from "./show"

export abstract class Edit extends Show {
    // BC aliases (read-only!)
    // todo: this should be removed and MUST be changed in all modules once we use the symfaurelia-commons in vwx.
    _getEntityData = this.getEntityData;
    _onEntitySaved = this.onEntitySaved;
    _onEntitySaveFailed = this.onEntitySaveFailed

    /**
     * Save entity, show toast and navigate to the show route / close dialog. See also writeEntity().
     */
    save(): void|Promise<any> {
        let itemBeforeSave = this.item;
        return this.writeEntity().then(() => {
            this.toaster.add("success", this.i18n.tr("crud:edit.success.title"), this.i18n.tr(itemBeforeSave ? "crud:edit.success.message.saved" : "crud:edit.success.message.created", {type: this.getReadableName()}));

            this.executeAfterAction(this.$config.afterSaveAction, this.item, true)

            this.eventAggregator.publish("entity:saved", {item: this.item});
            return this.item;
        });
    }

    /**
     * Write the entity to the backend.
     * @returns {Promise}
     */
    writeEntity() {
        return new Promise((resolve, reject) => {
            let url = this.$config.entityPath(this.item);
            let req = this.http.createRequest(url);
            let data = this.getEntityData();
            if (data instanceof FormData) {
                req.asPost();
                if (!this.isNew()) {
                    data.append('_method', 'put')
                }
            } else {
                if (this.isNew()) {
                    req.asPost();
                } else {
                    req.asPut();
                }
            }

            req.withContent(data).send().then(data => {
                this.item = data.content;
                this.onEntityLoaded();
                this.onEntitySaved();

                resolve(this.item);
                return data;
            }).catch(error => {
                this.onEntitySaveFailed(error);

                reject(error);
                return error;
            });
        });
    }

    /**
     * Get the entity data that should be written to the backend. Sanitize your fields here or return what you want.
     * @returns {*}
     * @protected
     */
    getEntityData() {
        return this.item;
    }

    /**
     * Optional life-cycle hook for when the entity was written to the backend successfully.
     * @protected
     */
    onEntitySaved() {
    }

    /**
     * Handle an error while saving the entity.
     * @param error
     * @protected
     * @abstract
     */
    onEntitySaveFailed(error) {
        throw Error("An error occurred while writing entity to backend and no error handler was declared.");
    }
}
