import {autoinject, BindingEngine, transient} from "aurelia-framework"
import {BindingSignaler} from "aurelia-templating-resources"
import {SubscriptionBag} from "symfaurelia/util/subscription-bag"

interface LoadedViewModel {
    id: string,
    rowId: string,
    inlineView: string,
    show: boolean,
    loading: boolean,
    model: any
    composeRef?: any
    composerRefBinding?: any
    promiseResolve: (any?) => void,
    promiseReject: () => void,
    promiseClose: (reason: CloseReason) => void,
}

export interface CloseReason {
    saved?: boolean
    deleted?: boolean
    closedByView?: boolean
    closedByRowClick?: boolean
}

export class WhenClosedPromise<T> extends Promise<T> {

    private whenClosedFulfilled = []

    constructor(
        executor: (
            resolve: (value: T | PromiseLike<T>) => void,
            reject: (reason?: any) => void,
            close: (reason: CloseReason) => void,
        ) => void,
    ) {

        super((resolve, reject) => {
            executor(resolve, reject, (reason: CloseReason) => {
                for (const fn of this.whenClosedFulfilled) {
                    fn(reason)
                }
            })
        })
    }

    public whenClosed(onfulfilled: (CloseReason) => void) {
        this.whenClosedFulfilled.push(onfulfilled)
        return this
    }
}

@autoinject()
@transient()
export class InlineTable {
    loadedViewModels: LoadedViewModel[] = []

    transitionTimeMs = 500 + 300 // 300 ms extra margin

    constructor(
        private bindingSignaler: BindingSignaler,
        private bindingEngine: BindingEngine,
        private subscriber: SubscriptionBag,
    ) {
        subscriber.subscribe('inline-table:close', (ev) => {
            this.close({closedByView: true, saved: ev.saved}, ev.vm)
        })
        subscriber.subscribe('entity:deleted', (ev) => {
            this.close({closedByView: true, deleted: true}, ev.vm)
        })
    }

    public dispose() {
        this.subscriber.unsubscribeAll()
        for (const loadedViewModel of this.loadedViewModels) {
            loadedViewModel.composerRefBinding.dispose()
        }
    }

    public openNew(inlineView: string) {
        this.open({}, inlineView, 'new')
    }

    public open(model, inlineView: string, rowId: string = null): WhenClosedPromise<void> {
        const id = model.id
        if (!rowId) {
            rowId = id
        }

        model[Symbol.for("inlineView")] = {
            mode: 'in-table',
            inlineTable: this,
            rowId,
        }

        return new WhenClosedPromise((resolve, reject, close) => {
            const rowIsOpen = this.rowIsOpen(rowId)
            this.close({closedByRowClick: true})
            if (
                !rowIsOpen ||
                (this.loadedViewModels.length > 0 && this.loadedViewModels[this.loadedViewModels.length - 1].inlineView != inlineView)
            ) {
                console.log('[InlineView] loading ' + id + ' / ' + inlineView)
                const newLoadedViewModel: LoadedViewModel = {
                    id,
                    rowId,
                    inlineView,
                    model,
                    show: false,
                    loading: true,
                    promiseResolve: resolve,
                    promiseReject: reject,
                    promiseClose: close,
                }

                setTimeout(() => {
                    if (!newLoadedViewModel.composeRef) {
                        console.warn('Did not get a compose reference within 1 second. There may be something wrong with your <compose>')
                    }
                }, 1000)

                // wait for aurelia to set the reference to the <compose>
                newLoadedViewModel.composerRefBinding = this.bindingEngine
                    .propertyObserver(newLoadedViewModel, 'composeRef')
                    .subscribe((newValue) => {
                        if (newValue) {
                            // if needed, wait until the <compose> has all tasks finished
                            if (newValue.pendingTask) {
                                newValue.pendingTask.then(() => this.show(newValue.currentViewModel))
                            } else {
                                this.show(newValue.currentViewModel)
                            }
                        }
                    })

                this.loadedViewModels.push(newLoadedViewModel)
                this.bindingSignaler.signal('inline-table:loaded-changed')
                this.bindingSignaler.signal('inline-table:open-changed')
            }
        })
    }

    public show(vm = null) {
        for (const loadedViewModel of this.loadedViewModels) {
            if ((vm === null && loadedViewModel.loading) || (loadedViewModel.composeRef && vm === loadedViewModel.composeRef.currentViewModel)) {
                console.log('[InlineView] opening ' + loadedViewModel.id + ' / ' + loadedViewModel.inlineView)
                loadedViewModel.loading = false
                loadedViewModel.show = true
                loadedViewModel.promiseResolve()
            }
        }
    }

    public close(reason: CloseReason, vm = null) {
        for (const loadedViewModel of this.loadedViewModels) {
            if (!vm || vm === loadedViewModel.composeRef.currentViewModel) {
                console.log('[InlineView] closing ' + loadedViewModel.rowId)
                loadedViewModel.loading = false
                loadedViewModel.show = false
                loadedViewModel.composerRefBinding.dispose()
                setTimeout(() => {
                    console.log('[InlineView] unloading ' + loadedViewModel.id + ' / ' + loadedViewModel.inlineView)
                    this.loadedViewModels.splice(this.loadedViewModels.indexOf(loadedViewModel), 1)
                    this.bindingSignaler.signal('inline-table:loaded-changed')
                    loadedViewModel.promiseClose(reason)
                }, this.transitionTimeMs)
            }
        }
        this.bindingSignaler.signal('inline-table:open-changed')
    }

    public rowIsOpen(rowId): boolean {
        for (const loadedViewModel of this.loadedViewModels) {
            if (loadedViewModel.rowId == rowId && (loadedViewModel.loading || loadedViewModel.show)) return true
        }
        return false
    }

    public hasLoadedRows() {
        return this.loadedViewModels.length > 0
    }
}
