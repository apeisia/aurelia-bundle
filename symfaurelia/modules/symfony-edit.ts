import {Edit} from "./edit"
import {debug} from "symfaurelia/log"
import {encodeQueryString} from "../util"

export abstract class SymfonyEdit extends Edit {
    formValues: any = {}
    formFiles: any = {}
    formFields: any = {}
    formSelects: any = {}
    formSelectData: any = {}
    formChildForms: any = {}
    formError
    loadSelectMarker

    // BC aliases (read-only!)
    // todo: this should be removed and MUST be changed in all modules once we use the symfaurelia-commons in vwx.
    _refreshFormFields = this.refreshFormFields
    _copyFormValues = this.copyFormValues
    _getEntityData = this.getEntityData
    _onEntitySaved = this.onEntitySaved
    _onEntitySaveFailed = this.onEntitySaveFailed
    _loadSelectEntires = this.loadSelectEntries

    async attached(): Promise<void> {
        // copy all named values of the form (if any) to the formValues map
        // so that only the values in the form get posted in the save() action

        this.refreshFormFields()
        if (this.loadSelectMarker) {
            await this.loadSelectEntries().then(() => {
                // firefox problem: wait until the new entries are rendered
                // so that this.formFields is correct
                setTimeout(() => {
                    this.refreshFormFields()
                }, 100)
            })
        }
        super.attached()
    }

    refreshFormFields() {
        if (this.item) {
            this.copyFormValues(this.formFields, this.item, this.formValues)
        } else {
            // new entity. we have problems with selects: they're missing the default value.
            const handleSelects = (fields, values) => {
                for (let name of Object.keys(fields)) {
                    if (fields[name].tagName === "SELECT") {
                        let active = fields[name].querySelector('option:checked')
                        if (!active && fields[name].children && fields[name].children.length > 0) {
                            active = fields[name].children()[0]
                        }
                        if (active && values) {
                            values[name] = active.model || null
                        }
                    } else if (!fields[name].tagName && typeof fields[name] === "object") {
                        handleSelects(fields[name], values[name])
                    }
                }
            }

            handleSelects(this.formFields, this.formValues)
        }
        // console.debug("child forms: ", this.formChildForms, this.formValues)
        for (const childForm of Object.keys(this.formChildForms)) {
            // i know that this is needed for child forms, for some reason. but i can't explain why we need to
            // check if the field exists on formFields. this class is a mess and fundamentally broken.
            if (this.formFields[childForm]) {
                this.formFields[childForm][Symbol.for('isChildForm')] = true
            }
        }

        this.app.eventAggregator.publish('form:refreshed')
    }


    /**
     * this function is called by a child that has changed the markup of its fields
     */
    formChangedByChildElement() {
        this.refreshFormFields()
    }

    copyFormValues(fields, source, target) {
        for (let name of Object.keys(fields)) {
            if (name in source && source[name] !== undefined) {

                if (source[name].id && fields[name].tagName === "SELECT") {
                    target[name] = source[name].id
                } else if (fields[name].tagName === "INPUT" && fields[name].getAttribute('type') == 'date') {
                    target[name] = source[name].split('T')[0]
                } else if (fields[name].tagName === "INPUT" && (
                    fields[name].getAttribute('type') == 'datetime' ||
                    fields[name].getAttribute('type') == 'datetime-local'
                )) {
                    target[name] = source[name].split('+')[0]
                } else if (
                    fields[name].date && fields[name].date.tagName === "INPUT" &&
                    fields[name].time && fields[name].time.tagName === "INPUT") {
                    target[name] = {
                        date: source[name].split('T')[0],
                        time: source[name].split('T')[1].split('+')[0].substr(0, 5),
                    }
                    // set data on field for webshim
                    fields[name].date.value = target[name].date
                    fields[name].time.value = target[name].time
                } else if (fields[name].tagName === "INPUT" && fields[name].getAttribute('data-convert-factor')) {
                    target[name] = parseFloat(source[name]) / parseFloat(fields[name].getAttribute('data-convert-factor'))
                } else if (fields[name] === true) {
                    // manually set field, this is mostly used for "config" objects that must be shallow copied
                    target[name] = source[name]
                } else if (Array.isArray(source[name])) {
                    target[name] = []
                    // extract ids if source is a array of objects
                    let src = []
                    for (let s of source[name]) {
                        if (typeof s === "object" && s.id != undefined) {
                            src.push(s.id)
                        } else {
                            src.push(s)
                        }
                    }
                    for (let k of Object.keys(fields[name])) {
                        if (k.startsWith('__i')) { // magic prefix for expanded multiple selects (=checkboxes)
                            if (Array.isArray(target[name])) {
                                delete target[name]
                                target[name] = {}
                            }
                            target[name][k] = src.includes(fields[name][k].value)
                        }
                        if (k.startsWith('__col')) { // magic prefix for collections
                            if (Array.isArray(target[name])) {
                                delete target[name]
                                target[name] = {}
                            }
                            target[name][k] = source[name][parseInt(k.substr(5))]
                        }
                    }
                    // still an array (changed to object by __i)?
                    if (Array.isArray(target[name])) {
                        for (let subitem of source[name]) {
                            target[name].push(subitem)
                        }
                    }
                } else if (typeof source[name] === "object") {
                    // got multiple form inputs for this key, most likely child form, copy recursively
                    target[name] = {}
                    debug("[FORM] Copying values recursively for", name)
                    this.copyFormValues(fields[name], source[name], target[name])
                } else {
                    target[name] = source[name]
                }
                if (fields[name].tagName === "INPUT" && ['date', 'datetime', 'month', 'number', 'time', 'range', 'color', 'datetime-local'].includes(fields[name].getAttribute('type'))) {
                    // fields that can be shimmed by webshim. The value.bind is somehow broken in this case - write the value directly
                    fields[name].value = target[name]
                }
            }
        }
    }

    getEntityData(forceFormData = false): { [key: string]: any } {
        let useFormData = forceFormData || Object.keys(this.formFiles).length > 0
        let decodeFields = [] // field for AureliaBundle::KernelListener to know which fields need to be deserialized in FormData mode
        let data
        if (useFormData) {
            data = new FormData()
        } else {
            data = {}
        }

        for (let k of Object.keys(this.formFiles)) {
            if (this.formFiles[k]) {
                for (let f of this.formFiles[k]) {
                    data.append(k, f)
                }
            }
        }

        const handleField = (formValues, formFields, breadcrumb, result) => {
            for (let k of Object.keys(formFields)) {

                let v = formValues[k]
                let field = formFields[k]

                if (field.tagName === "INPUT" && ['date', 'datetime', 'month', 'number', 'time', 'range', 'color', 'datetime-local'].includes(field.getAttribute('type'))) {
                    // fields that can be shimmed by webshim. The value.bind is somehow broken in this case - read the value directly
                    v = field.value
                }
                if (
                    field.date && field.date.tagName === "INPUT" &&
                    field.time && field.time.tagName === "INPUT") {
                    // webshim for seperate date + time fields
                    v = {
                        date: field.date.value,
                        time: field.time.value,
                    }
                } else if (field.tagName === "INPUT" && field.getAttribute('data-convert-factor')) {
                    v = parseFloat(field.value) * parseFloat(field.getAttribute('data-convert-factor'))
                } else if (typeof v === 'object' && v !== null) {
                    if (field[Symbol.for('isCollection')]) {
                        console.debug('collection detected: ', [...breadcrumb, k])

                        v = {}
                        for (let objk of Object.keys(field)) {
                            if (objk.startsWith('__col')) {
                                let i = objk.substr(5)
                                v[i] = {}
                                handleField(formValues[k][objk], field[objk], [...breadcrumb, k, objk], v[i])
                            }
                        }

                    } else if (field[Symbol.for('isChildForm')]) {
                        console.debug('child form detected: ', [...breadcrumb, k])

                        result[k] = {}
                        handleField(v, field, [...breadcrumb, k], result[k])

                    } else {
                        // deep copy
                        let copy = JSON.parse(JSON.stringify(v))
                        for (let objk of Object.keys(v)) {
                            if (objk.startsWith('__i')) { // magic prefix for expanded multiple selects (=checkboxes)
                                if (copy[objk]) {
                                    copy[objk.substr(3)] = field[objk].value
                                }
                                delete copy[objk]

                                v = copy
                            }
                        }
                    }
                }
                if (useFormData && breadcrumb.length === 0) {
                    // checkboxes are not submitted when not selected.
                    // So the form only checks whether it checkbox name is in the request (basically doing isset()).
                    // TODO when having problems because of this, do a better check
                    if (v !== false && v !== undefined && v !== null) {
                        if (typeof v === 'object') {
                            v = JSON.stringify(v)
                            decodeFields.push([...breadcrumb, k].join('.'))
                        }
                        formValues[k] = v
                        data.append(buildParamPath([...breadcrumb, k]), v)
                    }
                } else {
                    formValues[k] = v
                    result[k] = v
                }
            }
        }

        handleField(JSON.parse(JSON.stringify(this.formValues)), this.formFields, [], data)

        if (useFormData) {
            data.append('_decode', JSON.stringify(decodeFields))
        }
        return data
    }

    onEntitySaved() {
        this.refreshFormFields()
    }

    onEntitySaveFailed(error) {
        if (error.statusCode === 400) {
            this.formError = error.content.form
            if (!this.formError.children && this.formError.errors && this.formError.errors.children) {
                // newer form component version?
                this.formError.children = this.formError.errors.children
            }
        }
    }

    loadSelectEntries() {
        return new Promise((resolve, reject) => {
            let queryParamsStr = ""
            let queryParams = this.getFormRequestParams()
            if (queryParams) {
                queryParamsStr = "?" + encodeQueryString(queryParams)
            }

            this.http.get('/form/' + this.getModuleId() + queryParamsStr).then(data => {
                this.formSelects = data.content
                this.formSelectData = {}

                for (const select in this.formSelects) {
                    if (!this.formSelects.hasOwnProperty(select)) continue

                    for (const data of this.formSelects[select]) {
                        this.formSelectData[select] ??= {}
                        this.formSelectData[select][data.value] = data
                    }
                }

                resolve(this.formSelects)
                return data
            }).catch(error => {
                reject(error)
                return error
            })
        })
    }

    getModuleId() {
        return this.router.currentInstruction.config.moduleId
    }

    getFormRequestParams(): any {
        return []
    }

    defineExpandedSelect(id) {
        const parts = id.split('.')

        let f = this.formFields
        let v = this.formValues

        for (const part of parts) {
            f[part] = {}

            if (!v[part]) {
                v[part] = {}
            }

            f[Symbol.for('isChildForm')] = true

            f = f[part]
            v = v[part]
        }
    }

    elementInvalid(name, ev) {
        // TODO call callback on config element. currently only used in translatable.ts
        return true
    }
}

function buildParamPath(path) {
    if (!path.length) {
        return null
    }

    let paramKey = path[0]

    for (let i = 1; i < path.length; ++i) {
        if (path[i].substr(-2) === '[]') {
            paramKey += '[' + path[i].substr(0, path[i].length - 2) + '][]'
        } else {
            paramKey += '[' + path[i] + ']'
        }
    }

    return paramKey
}
