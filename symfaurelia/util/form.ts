/**
 * Creates a copy of the given object with only the properties that are listed in fields.
 *
 * @param object
 * @param fields
 * @returns object
 */
export function filterObject(object, fields) {
    let data = {};

    for (let field of fields) {
        data[field] = object[field];
    }

    return data;
}

export function filterArrayObjects(arr, fields) {
    let data = [];

    for (let item of arr) {
        data.push(filterObject(item, fields));
    }

    return data;
}
