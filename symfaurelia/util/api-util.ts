// thanks to php stupid array concept objects that are returned from an api will sometimes be an empty array. fix that
export function forceObject(object) {
    if (Array.isArray(object) && object.length === 0) {
        return {};
    }

    return object;
}
