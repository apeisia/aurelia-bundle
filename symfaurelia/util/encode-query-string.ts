import * as querystring from "querystring";


// taken from https://stackoverflow.com/questions/1714786/query-string-encoding-of-a-javascript-object
// export function encodeQueryString(obj, prefix = undefined) {
//     let str = [];
//     let p;
//
//     for (p in obj) {
//         if (obj.hasOwnProperty(p)) {
//             let k = prefix ? prefix + "[" + p + "]" : p,
//                 v = obj[p];
//             str.push((v !== null && typeof v === "object") ?
//                 encodeQueryString(v, k) :
//                 encodeURIComponent(k) + "=" + encodeURIComponent(v));
//         }
//     }
//
//     return str.join("&");
// }

export function encodeQueryString(obj) {
    return querystring.stringify(obj);
}

export function decodeQueryString(str) {
    let queryString = location.search.substr(0, 1) == "?" ? location.search.substr(1) : location.search;

    return querystring.parse(queryString);
}
