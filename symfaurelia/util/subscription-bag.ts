import {EventAggregator, Subscription} from "aurelia-event-aggregator";
import {autoinject, transient} from "aurelia-framework";

@autoinject()
@transient()
export class SubscriptionBag {
    items : Subscription[] = [];

    constructor(private ea: EventAggregator) {
    }

    add(sub: Subscription) {
        this.items.push(sub);

        return sub;
    }

    subscribe(event, callback) {
        return this.add(this.ea.subscribe(event, callback));
    }

    unsubscribeAll() {
        for (let s of this.items) {
            s.dispose();
        }
    }
}
