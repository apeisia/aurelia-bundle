export function getClassForType(type) {
    if (["success", "info", "warning"].indexOf(type) !== -1) {
        return "alert-" + type;
    }

    return "alert-danger";
}

export class Toaster {
    toasts = [];

    /**
     * Type can be "success", "warning" or "error".
     * Decay is the number of milliseconds after which the toast gets closed automatically.
     *
     * @param type
     * @param title
     * @param message
     * @param decay
     * @param buttons
     */
    add(type: "success" | "info" | "warning" | "error", title: string, message: string, decay = null, buttons = null) {
        if (decay === null && type === "success") {
            decay = 5000;
        }

        let toast = {
            "type": type,
            "title": title,
            "message": message,
            "alert_class": getClassForType(type),
            "decay": decay,
            "buttons": buttons
        };

        this.toasts.push(toast);

        if (decay) {
            setTimeout(() => {
                this.remove(toast);
            }, decay)
        }

        return toast;
    }

    remove(toast) {
        let idx = this.toasts.indexOf(toast);

        if (idx >= 0) {
            this.toasts.splice(idx, 1);
        }
    }

    clear() {
        this.toasts.splice(0);
    }
}
