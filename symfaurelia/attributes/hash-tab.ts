/**
 * Create tab bars and similar behaviour easily.
 *
 * On the tab buttons put:
 *   hash-tab-link="<tab-name>"
 *
 * The tab that should be enabled by default must be given the "active" css class. The `hash-tab-link` custom
 * attribute will automatically set and remove the "active" class when changing tabs.
 *
 * On the individual elements that should be swapped by the tabs put:
 *   hash-tab="<tab-name>"
 *
 * This will act like a `show.bind` (i.e., it will add or remove the "aurelia-hide" css class).
 * If you need the element to be removed from the DOM, like `if.bind` use `hash-tab-if` instead.
 *
 * -----------------
 *   *Important*:
 * -----------------
 *
 * By default, the hash tabs will use a group named "default". If you have more than one pair of tabs on a single
 * page you must define a group for the tabs, like so:
 *   hash-tab-link="<tab-name>; group: <group-name>"
 *   hash-tab="<tab-name>; group: <group-name>"
 *
 * Of course, you can dynamically provide a group with `group.bind`:
 *   hash-tab="<tab-name>; group.bind: <group-name-var>"
 */
import {autoinject, bindable, noView, templateController} from "aurelia-framework"
import {EventAggregator} from "aurelia-event-aggregator"
import {BoundViewFactory, ViewFactory, ViewSlot} from "aurelia-templating"
import {SubscriptionBag} from "symfaurelia/util/subscription-bag"
import {decodeQueryString, encodeQueryString} from "symfaurelia/util"

@autoinject()
@noView()
export class HashTabActiveManager {
    items = {}

    constructor(private ea: EventAggregator) {
        window.addEventListener("popstate", ev => {
            this.updateActiveTabs()
        })

        ea.subscribe("router:navigation:processing", ev => {
            this.updateActiveTabs()
        })
    }

    updateActiveTabs() {
        for (let group of Object.keys(this.items)) {
            let queryParams = decodeQueryString(location.search)
            if (queryParams["_tab_" + group]) {
                let tab = !!queryParams["_tab_" + group]

                if (tab != this.isActive(group, tab)) {
                    this.setActive(group, tab)
                    this.ea.publish("hashtab:changed", {"group": group, "tab": tab})
                }
            }
        }
    }

    setActive(group, tab) {
        this.items[group] = tab
    }

    isActive(group, tab) {
        return this.getActive(group) == tab
    }

    getActive(group) {
        return this.items[group]
    }

    navigate(group, tab) {
        this.ea.publish("hashtab:changed", {"group": group, "tab": tab})

        let queryParams = decodeQueryString(location.search)
        queryParams["_tab_" + group] = tab

        history.replaceState(null, null, location.pathname + "?" + encodeQueryString(queryParams))
    }
}

@autoinject()
export class HashTabCustomAttribute {
    @bindable({primaryProperty: true})
    tab
    @bindable()
    group = "default"

    constructor(private el: Element, private ea: EventAggregator, private subscriptions: SubscriptionBag, private manager: HashTabActiveManager, private viewFactory: ViewFactory, private viewSlot: ViewSlot) {
    }

    attached() {
        this.el.classList.add("aurelia-hide")

        this.subscriptions.subscribe("hashtab:changed", data => {
            if (data.group != this.group) {
                return
            }

            if (data.tab == this.tab) {
                this.el.classList.remove("aurelia-hide")
            } else {
                this.el.classList.add("aurelia-hide")
            }
        })

        if (this.manager.isActive(this.group, this.tab)) {
            this.el.classList.remove("aurelia-hide")
        }
    }

    detached() {
        this.subscriptions.unsubscribeAll()
    }
}

@autoinject()
@templateController
export class HashTabIfCustomAttribute {
    @bindable({primaryProperty: true})
    tab
    @bindable()
    group = "default"

    bindingContext: any
    overrideContext: any
    view: any = null
    showing: boolean = false
    cache: boolean | string = true

    constructor(private manager: HashTabActiveManager,
                private ea: EventAggregator,
                private subscriptions: SubscriptionBag,
                private viewFactory: BoundViewFactory,
                private viewSlot: ViewSlot) {
    }

    bind(bindingContext, overrideContext) {
        this.bindingContext = bindingContext
        this.overrideContext = overrideContext
    }

    unbind() {
        if (this.view === null) {
            return
        }

        this.view.unbind()

        if (!this.viewFactory.isCaching) {
            return
        }

        if (this.showing) {
            this.showing = false
            this.viewSlot.remove(this.view, true, true)
        } else {
            this.view.returnToCache()
        }

        this.view = null
    }

    /**
     * @internal
     */
    _show() {
        if (this.showing) {
            // Ensures the view is bound.
            // It might not be the case when the if was unbound but not detached, then rebound.
            // Typical case where this happens is nested ifs
            if (!this.view.isBound) {
                this.view.bind(this.bindingContext, this.overrideContext)
            }
            return
        }

        if (this.view === null) {
            this.view = (this.viewFactory as any).create()
        }

        if (!this.view.isBound) {
            this.view.bind(this.bindingContext, this.overrideContext)
        }

        this.showing = true
        return this.viewSlot.add(this.view) // Promise or void
    }

    /**
     * @internal
     */
    _hide() {
        if (!this.showing) {
            return
        }

        this.showing = false
        let removed = this.viewSlot.remove(this.view) // Promise or View

        if (removed instanceof Promise) {
            return removed.then(() => {
                this._unbindView()
            })
        }

        this._unbindView()
    }

    /**
     * @internal
     */
    _unbindView() {
        const cache = this.cache === 'false' ? false : !!this.cache
        this.view.unbind()
        if (!cache) {
            this.view = null
        }
    }

    attached() {
        this.subscriptions.subscribe("hashtab:changed", data => {
            if (data.group != this.group) {
                return
            }

            if (data.tab == this.tab) {
                this._show()
            } else {
                this._hide()
            }
        })

        if (this.manager.isActive(this.group, this.tab)) {
            this._show()
        }
    }

    detached() {
        this.subscriptions.unsubscribeAll()
    }
}

@autoinject()
export class HashTabLinkCustomAttribute {
    @bindable({primaryProperty: true})
    tab
    @bindable()
    group = "default"

    clickListener

    constructor(private el: Element, private ea: EventAggregator, private subscriptions: SubscriptionBag, private manager: HashTabActiveManager) {
    }

    attached() {
        this.clickListener = e => {
            e.stopPropagation()
            e.preventDefault()

            this.manager.navigate(this.group, this.tab)

            return false
        }

        this.el.addEventListener("click", this.clickListener)

        this.subscriptions.subscribe("hashtab:changed", data => {
            if (data.group != this.group) {
                return
            }

            if (data.tab == this.tab) {
                this.el.classList.add("active")
                this.manager.setActive(this.group, this.tab)
            } else {
                this.el.classList.remove("active")
            }
        })

        // check active via css class on link
        if (this.el.classList.contains("active")) {
            this.manager.setActive(this.group, this.tab)
        }

        // check active via url
        let queryParams = decodeQueryString(location.search)
        if (queryParams["_tab_" + this.group] && queryParams["_tab_" + this.group] == this.tab) {
            this.manager.setActive(this.group, this.tab)
            this.ea.publish("hashtab:changed", {"group": this.group, "tab": this.tab})
        }
    }

    detached() {
        this.el.removeEventListener("click", this.clickListener)
        this.subscriptions.unsubscribeAll()
    }
}
