import {autoinject, customAttribute} from "aurelia-framework"

@autoinject()
@customAttribute("classes")
export class ClassesCustomAttribute {
    value;

    constructor(private element: Element) {
    }

    valueChanged() {
        for (const classNames of Object.keys(this.value)) {
            for (const className of classNames.split(' ')) {
                this.element.classList.toggle(className, !!this.value[classNames]);
            }
        }
    }
}
