import {autoinject} from "aurelia-framework";

@autoinject()
export class SpinnerCustomAttribute {

    child: HTMLDivElement;

    constructor(private element: Element) {
    }

    show() {
        this.child = document.createElement("div");
        this.child.classList.add('wait-loading');
        let spin = document.createElement('i');
        spin.classList.add('fa', 'fa-spinner', 'fa-spin');
        this.child.appendChild(spin);
        this.element.appendChild(this.child);
        this.element.classList.add('wait-loading-parent');
        (<HTMLButtonElement>this.element).disabled = true;
    }

    hide() {
        try {
            this.element.removeChild(this.child);
        } catch (e) {}
        this.element.classList.remove('wait-loading-parent');
        (<HTMLButtonElement>this.element).disabled = false;
    }

    valueChanged(value, oldValue) {
        if (value instanceof Promise) {
            this.hide();
            this.show();
            let finish = (a) => {
                this.hide();
                return a;
            };
            value.then(finish).catch(finish);
        }
        else {
            // use the value directly as an indicator if we are fully loaded
            if (value) {
                this.hide();
            } else {
                this.show();
            }
        }
    }
}
