import {bindable, observable} from "aurelia-framework";

export const STRENGTH = {
    VERY_WEAK: 0,
    WEAK: 1,
    MEDIUM: 2,
    GOOD: 3,
    STRONG: 4,
    VERY_STRONG: 5
};

export const LIMIT_FACTOR = {
    LENGTH: "length",
    MISSING_SYMBOLS: "symbols",
    MISSING_CASE: "capitals_minuscules",
    MISSING_NUMBERS: "numbers"
};

function limitResultTo(result, factor, maxStrength) {
    result.limitedBy.push(factor);
    result.limitFactors[factor] = maxStrength;
}

export class PasswordEvaluator {
    analyse(password) {
        let appearedCharacters = [];
        let result = {
            input: password,
            length: password.length,
            numbers: 0,
            minuscules: 0,
            capitals: 0,
            symbols: 0,
            unique: 0
        };

        for (let i = 0; i < password.length; ++i) {
            let character = password[i];

            if ("abcdefghijklmnopqrstuvwxyz".indexOf(character) >= 0) {
                result.minuscules++;
            } else if ("ABCDEFGHIJKLMNOPQRSTUVWXYZ".indexOf(character) >= 0) {
                result.capitals++;
            } else if ("01213456789".indexOf(character) >= 0) {
                result.numbers++;
            } else {
                result.symbols++;
            }

            if (!appearedCharacters.includes(character)) {
                appearedCharacters.push(character);
                result.unique++;
            }
        }

        return result;
    }

    valuate(passwordOrAnalysis) {
        let analysis = this._getPasswordOrAnalysis(passwordOrAnalysis);
        let maxStrength = 5;
        let score = 0;
        let result = {
            strength: null,
            score: null,
            limitedBy: [],
            limitFactors: {},
            input: analysis.input,
            analysis: analysis
        };
        let symbolPercentage = analysis.symbols / analysis.length;

        // score by the amount of characters included in the password
        score += analysis.symbols * 3;
        score += analysis.capitals * 2;
        score += analysis.minuscules * 1.5;
        score += analysis.numbers;

        // punish repeating characters
        score *= analysis.length / analysis.unique;

        // punish missing characters by limiting the maximum strength
        if (analysis.symbols === 0) {
            maxStrength--;
            limitResultTo(result, LIMIT_FACTOR.MISSING_SYMBOLS, maxStrength);
        }

        if (symbolPercentage < 0.5) {
            // allow missing capitals, minuscules and numbers if we have a good percentage of symbols
            if (analysis.capitals === 0 || analysis.minuscules === 0) {
                maxStrength--;
                limitResultTo(result, LIMIT_FACTOR.MISSING_CASE, maxStrength);
            }

            if (analysis.numbers === 0) {
                maxStrength--;
                limitResultTo(result, LIMIT_FACTOR.MISSING_NUMBERS, maxStrength);
            }
        }

        // punish very short passwords
        if (analysis.length < 4) {
            maxStrength = STRENGTH.VERY_WEAK;
            limitResultTo(result, LIMIT_FACTOR.LENGTH, maxStrength);
        }

        // valuate the score
        result.score = score;
        result.strength = STRENGTH.VERY_WEAK;

        if (result.score >= 10) {
            result.strength = STRENGTH.WEAK;
        }

        if (result.score >= 15) {
            result.strength = STRENGTH.MEDIUM;
        }

        if (result.score >= 20) {
            result.strength = STRENGTH.GOOD;
        }

        if (result.score >= 25) {
            result.strength = STRENGTH.STRONG;
        }

        if (result.score >= 35) {
            result.strength = STRENGTH.VERY_STRONG;
        }

        // limit the result strength to the previously determined maximum
        if (result.strength > maxStrength) {
            result.strength = maxStrength;
        }

        return result;
    }

    _getPasswordOrAnalysis(passwordOrAnalysis) {
        if (typeof passwordOrAnalysis !== "object") {
            return this.analyse(passwordOrAnalysis);
        }

        return passwordOrAnalysis;
    }
}

export class PasswordValuationCustomElement {
    valuator = new PasswordEvaluator();

    @bindable()
    @observable()
    password;

    strength = 0;

    passwordChanged(newValue) {
        this.strength = this.valuator.valuate(newValue).strength;
    }
}
