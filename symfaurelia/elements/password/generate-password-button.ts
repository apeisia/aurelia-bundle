import {bindable} from "aurelia-framework";
import {generateRandomString} from "symfaurelia/util/random-string";

function generatePassword() {
    return generateRandomString(12, "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789?!%_-.=;:()@+*#'$");
}

export class GeneratePasswordButtonCustomElement {
    @bindable()
    callback;

    generate() {
        this.callback({newPassword: generatePassword()});
    }
}
