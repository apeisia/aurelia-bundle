import {inject, bindable, InlineViewStrategy} from 'aurelia-framework';

export class PartCustomElement {

    @bindable view;
    @bindable model;
    viewStrategy;

    viewChanged() {
        if (this.view) {
            this.viewStrategy = new InlineViewStrategy(this.view);
        }
    }
}
