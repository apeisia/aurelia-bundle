import {autoinject, bindable, customElement} from "aurelia-framework";
import {Dropdown} from "symfaurelia/ui/dropdown";

@autoinject()
@customElement("au-dropdown")
export class DropdownCustomElement {
    @bindable({name: "menu-class"}) menuClass = "dropdown-menu";
    public dropdown = new Dropdown();

    close() {
        this.dropdown.close();
    }
}
