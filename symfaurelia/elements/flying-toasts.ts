import {autoinject} from "aurelia-framework";
import {Toaster} from "symfaurelia/toaster";

@autoinject()
export class FlyingToastsCustomElement {
    toaster: Toaster;

    constructor(toaster: Toaster) {
        this.toaster = toaster;
    }

    close(toast) {
        this.toaster.remove(toast);
    }

    buttonClicked(toast, button) {
        button.action();
        this.close(toast);
    }
}
