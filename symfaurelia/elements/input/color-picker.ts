import {bindable, bindingMode, observable} from "aurelia-framework";

export class ColorPickerCustomElement {
    @bindable()
    id;

    @bindable({defaultBindingMode: bindingMode.twoWay})
    @observable()
    value;

    @observable()
    text;

    valueChanged(nv, ov) {
        this.text = nv;
    }

    textChanged(nv, ov) {
        if (nv.substr(0, 1) != "#") {
            this.text = "#" + nv;
        }

        if (this.text.length == 7) {
            this.value = this.text;
        }
    }

    textUnfocused() {
        if (this.text.length == 4) {
            this.value = this.text + this.text.substr(1);
        }
    }
}
