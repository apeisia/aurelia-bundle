import {inject} from "aurelia-framework";

export class RequestTracker {

    list = [];

    /**
     *
     * @param request {HttpRequestMessage}
     */
    start(request) {
        if (!(<any>window).enableRequestTracker) return;
        let shorted = request.url;
        const max = 40;
        if(shorted.length > max) {
            let len = 18;
            shorted = shorted.substr(0, len) + '...' + shorted.substr(-len);
        }
        this.list.push({
            request: request,
            url: request.url,
            shorted: shorted,
            method: request.method,
        });
        if (this.list.length > 11) {
            this.list.splice(0, 1)
        }
    }

    /**
     *
     * @param response {HttpResponseMessage}
     */
    end(response) {
        if (!(<any>window).enableRequestTracker) return;

        for (let item of this.list) {
            if (item.request === response.requestMessage) {
                let code = response.statusCode;

                item.request = undefined;
                item.profiler = response.headers.get('x-debug-token-link');
                item.statusCode = code;
                item.statusCodeType = code - (code % 100);

                console.debug('[' + item.method + '] "' + item.url + '" -> ' + item.statusCode + ' ' + ( item.profiler ? item.profiler : '(no profiler)'));

                if (item.statusCode === 500) {
                    (<any>window).sfErrorHandler(item.profiler);
                }
            }
        }
    }
}


@inject(RequestTracker)
export class RequestTrackerCustomElement {
    reqtracker;

    enableRequestTracker;
    constructor(reqtracker) {
        this.reqtracker = reqtracker;
        this.enableRequestTracker = (<any>window).enableRequestTracker;
    }

    handler(item) {
        (<any>window).sfErrorHandler(item.profiler);
    }
}
