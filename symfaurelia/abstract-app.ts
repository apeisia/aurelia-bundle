import {ObserverLocator} from "aurelia-binding";
import {EventAggregator} from "aurelia-event-aggregator";
import {RequestTracker} from './debug/request-tracker';
import {I18N} from "aurelia-i18n";
import {Dialog} from "./dialog";
import {HttpClient} from "aurelia-http-client";
import {Router} from "aurelia-router";

export class AbstractApp {
    static inject() {
        return [HttpClient, EventAggregator, RequestTracker, ObserverLocator, I18N, Dialog];
    }

    ROUTE_APP_PREFIX = "/app";
    TITLE = "Symfaurelia App";

    http;
    eventAggregator;
    requestTracker;
    observerLocator;
    i18n;
    /** @type Router */
    router;
    runningHttpRequests = 0;
    frontendRev;

    /**
     * Contains the app config from the initial _app_config.json request (retrieved in `activate()`).
     */
    config;

    /**
     * @type Dialog
     */
    dialog;

    constructor(http, ea, rt, ol, i18n, dialog) {
        this.http = http;
        this.eventAggregator = ea;
        this.requestTracker = rt;
        this.observerLocator = ol;
        this.i18n = i18n;
        this.dialog = dialog;
        this.configureHttp();
    }

    configureHttp() {
        let reqtracker = this.requestTracker;
        let app = this;
        //this.http.rejectPromiseWithErrorObject = true;
        this.http.configure(x => {
            x.withBaseUrl('api/');
            x.withCredentials(true);
            x.withInterceptor({
                request(message) {
                    if (!message.__background__) app.runningHttpRequests++;
                    reqtracker.start(message);
                    return message;
                },
                requestError(error) {
                    app.runningHttpRequests--;
                    throw error;
                },
                response(message) {
                    if (!message.requestMessage.__background__) app.runningHttpRequests--;
                    reqtracker.end(message);

                    // check if the answer is a redirect to the login page and redirect the user if that is the case
                    let headers = message.headers.headers;
                    if (headers["x-security-page"] && headers["x-security-page"].value === "login") {
                        // todo change redirect based on the base url.
                        window.location.href = "/login";
                    }

                    // check if the frontend rev changed.
                    if (headers['x-frontend-rev'] && app.frontendRev && headers['x-frontend-rev'].value !== app.frontendRev) {
                        app.requestReload();
                    }
                    app.httpResponse(message);

                    return message;
                },
                responseError(error) {
                    if(!error.requestMessage) {
                        console.error(error);
                        return;
                    }
                    if (!error.requestMessage.__background__) app.runningHttpRequests--;
                    reqtracker.end(error);
                    app.httpResponseError(error);
                    throw error;
                }
            });
        });

        HttpClient.prototype.get = function get(url, params) {
            return this.createRequest(url).asGet().withParams(params).send();
        };

        let runBg = (req) => {
            req.transformers.push(function (client, processor, message) {
                message.__background__ = true;
            });
            return req.send();
        };

        (<any>HttpClient).prototype.getBg = function get(url, params) {
            return runBg(this.createRequest(url).asGet().withParams(params));
        };

        (<any>HttpClient).prototype.deleteBg = function get(url, params) {
            return runBg(this.createRequest(url).asDelete().withParams(params));
        };

        (<any>HttpClient).prototype.postBg = function get(url, content=null, params=null, progressCallback?: (event?: Event) => void) {
            let req =  this.createRequest(url).asPost();
            if(content) {
                req.withContent(content)
            }
            if(params) {
                req.withParams(params)
            }
            if (progressCallback) {
                req.withProgressCallback(progressCallback);
            }
            return runBg(req);
        };

        (<any>HttpClient).prototype.putBg = function get(url, content=null, params=null, progressCallback?: (event?: Event) => void) {
            let req = this.createRequest(url).asPut();
            if(content) {
                req.withContent(content)
            }
            if(params) {
                req.withParams(params)
            }
            if (progressCallback) {
                req.withProgressCallback(progressCallback);
            }
            return runBg(req);
        };
    }

    requestReload() {
        this.dialog.confirm("Update eingespielt", "Seit Ihrer letzten Aktion wurde ein Softwareupdate eingespielt."
            +" Wollen Sie die Seite neu laden, um das Update zu aktivieren?").then(() => {
            return location.reload(true);
        });
    }

    httpResponse(message) { }
    httpResponseError(error) { }
}
