import {EventAggregator} from "aurelia-event-aggregator";
import {ObserverLocator} from "aurelia-binding";
import {AppRouter} from "aurelia-router";
import {autoinject} from "aurelia-framework";

@autoinject()
export class UrlActiveMenuVoter {
    router: AppRouter;

    /**
     * Set this only by calling `configure()`!
     */
    _menuConfig;

    /**
     * The menu item that is considered to be the active one.
     */
    currentMenuItem;

    routableMenuItems;

    _routerConfigured: boolean = false;

    constructor(ea: EventAggregator, observerLocator: ObserverLocator, appRouter: AppRouter) {
        this.router = appRouter;

        // update currentMenuItem on router navigation
        observerLocator.getObserver(appRouter, "currentInstruction").subscribe((newValue, oldValue) => {
            this.update();
        });
    }

    configure(menuConfig) {
        this._menuConfig = menuConfig;

        if (this._routerConfigured) {
            this.routableMenuItems = this._prepareRoutableMenuItems(this._menuConfig);
        }

        this.update();
    }

    update() {
        const newValue = this.router.currentInstruction;

        if (!this._menuConfig) {
            return;
        }

        this._routerConfigured = true;
        this.routableMenuItems = this._prepareRoutableMenuItems(this._menuConfig);
        this.currentMenuItem = this.getActiveItemBestMatchByUrl(newValue.fragment);
    }

    getActiveItemBestMatchByUrl(currentUrl) {
        let bestMatch = {length: 0, item: null};

        for (let menuItem of this.routableMenuItems) {
            if (!currentUrl.startsWith(menuItem.url)) {
                continue;
            }

            if (currentUrl === menuItem.url) {
                return menuItem;
            }

            if (menuItem.url.length > bestMatch.length) {
                bestMatch.length = menuItem.url.length;
                bestMatch.item = menuItem;
            }
        }

        return bestMatch.item;
    }

    /**
     * Creates a flat array of all menu items that have a route associated. This is required to find the best
     * matching active menu element for highlighting it.
     *
     * @param menu
     * @returns {Array}
     * @private
     */
    _prepareRoutableMenuItems(menu) {
        let elements = [];

        for (let item of menu) {
            if (item.children && item.children.length > 0) {
                elements = elements.concat(this._prepareRoutableMenuItems(item.children));
            }

            if (item.route) {
                item.url = this.router.generate(item.route, item.params);

                // required if navigation is not done via pushState()
                if (item.url.startsWith("#")) {
                    item.url = item.url.substr(1);
                }

                elements.push(item);
            }
        }

        return elements;
    }
}
