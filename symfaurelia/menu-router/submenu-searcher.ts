export function elementsContainElement(element, searchedElement) {
    if (element.route === searchedElement.route && element.params == searchedElement.params) {
        return true;
    }

    if (element.children && element.children.length) {
        for (let child of element.children) {
            if (elementsContainElement(child, searchedElement)) {
                return true;
            }
        }
    }

    return false;
}
