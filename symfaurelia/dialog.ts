import {YesNoDialog} from "./dialogs/yes-no"
import {DialogController, DialogService} from "aurelia-dialog"
import {autoinject, PLATFORM} from "aurelia-framework"
import {I18N} from "aurelia-i18n"

export interface DialogButton {
    label: string;
    icon?: string;
    translate?: boolean;
    handler: (ctrl?: DialogController, text?: string) => void;
    class?: string;
}

export interface AdditionalDialogOptions {
    type?: "success" | "error" | "progress" | "info" | "question" | "loading" | null,
    autoClose?: false | number,
    translate?: boolean,
    buttons?: DialogButton[],
    locked?: boolean,
    dialogClass?: string,
    headerClass?: string;
    icon?: string;
    progress?: {
        // obviously only for progress type
        running: boolean,
        error: boolean,
        progress: number,
    },
}

export interface DialogOptions extends AdditionalDialogOptions {
    title: string,
    message: string | Element,
}

type PromptInputType = "textarea" | "text" | "date" | "datetime" | "number"

export interface PromptOptions {
    inputType?: PromptInputType;
    defaultText?: string;

    bottomText?: string;
}

function optionsOrIconToOptions(val) {
    if (typeof val == "string") {
        return {
            icon: val,
        }
    }

    return val
}

@autoinject
export class Dialog {
    static BUTTON_OK: DialogButton = {
        label: "dialog.button.ok",
        translate: true,
        class: "btn btn-primary",
        handler: (ctrl, text) => ctrl.ok(text),
    }
    static BUTTON_YES: DialogButton = {
        label: "dialog.button.yes",
        translate: true,
        class: "btn btn-primary btn-raised",
        handler: ctrl => ctrl.ok(true),
    }
    static BUTTON_NO: DialogButton = {
        label: "dialog.button.no",
        translate: true,
        class: "btn btn-secondary",
        handler: ctrl => ctrl.ok(false),
    }
    static BUTTON_CANCEL: DialogButton = {
        label: "dialog.button.cancel",
        translate: true,
        class: "btn btn-secondary",
        handler: ctrl => ctrl.cancel(),
    }

    constructor(public service: DialogService, private i18n: I18N) {
    }

    open(options: DialogOptions) {
        options = normalizeOptions(options)

        console.log("options: ", options)

        return new Promise((resolve, reject) => {
            this.service.open({
                viewModel: PLATFORM.moduleName("symfaurelia/dialogs/generic"),
                view: PLATFORM.moduleName("symfaurelia/dialogs/generic.html"),
                model: {
                    "options": options,
                },
                lock: options.locked,
                rejectOnCancel: true,
            }).whenClosed(result => {
                if (result.wasCancelled) {
                    reject(result.output)
                } else {
                    resolve(result.output)
                }
            }).catch(reject)
        })
    }

    async alert(title, message, options: AdditionalDialogOptions = {}) {
        if (!message) {
            message = title
            title = this.i18n.tr("dialog.confirmation")
        }
        let opt = Object.assign({
            title, message,
            type: "info",
            buttons: [Dialog.BUTTON_OK],
        }, options)

        return await this.open(opt)
    }

    /**
     *
     * @param title
     * @param message
     * @param optionsOrIcon
     * @deprecated use confirmToBool
     */
    confirm(title, message, optionsOrIcon: any = "question-circle"): Promise<void> {
        if (!message) {
            message = title
            title = this.i18n.tr("dialog.confirmation")
        }

        return new Promise((resolve, reject) => {
            this.service.open({
                viewModel: YesNoDialog,
                view: "symfaurelia/dialogs/yes-no.html",
                model: {
                    "title": title,
                    "message": message,
                    "options": optionsOrIconToOptions(optionsOrIcon),
                },
            }).whenClosed(response => {
                if (response.wasCancelled) {
                    reject()
                } else {
                    resolve(null)
                }
            }).catch(reject)
        })
    }

    async confirmToBool(title, message, options: AdditionalDialogOptions = {}) {
        return await this.open(Object.assign({
            title: title,
            message: message,
            translate: true,
            buttons: [Dialog.BUTTON_NO, Dialog.BUTTON_YES],
            type: "question",
        }, options))
    }

    /**
     * Allow inputType = "input" for bc (use "text" instead)
     *
     * @param title
     * @param message
     * @param inputType
     * @param optionsOrIcon
     * @param lock
     * @deprecated use prompt() instead
     */
    async input(title, message, inputType: PromptInputType | "input" = "text", optionsOrIcon: any = "question-circle", lock: boolean = false) {
        if (inputType == "input") {
            inputType = "text"
        }

        return await this.prompt(title, message, undefined, {
            inputType: inputType,
        })
    }

    prompt(title: string, message: string, placeholder: string = undefined, options: PromptOptions = undefined) {
        return new Promise((resolve, reject) => {
            this.service.open({
                viewModel: PLATFORM.moduleName("symfaurelia/dialogs/prompt"),
                view: PLATFORM.moduleName("symfaurelia/dialogs/prompt.html"),
                model: {
                    "options": {
                        title: title,
                        message: message,
                        placeholder: placeholder,
                        translate: true,
                        buttons: [Dialog.BUTTON_CANCEL, Dialog.BUTTON_OK],
                        type: "question",
                        inputType: (options ? options.inputType : null) || "text",
                        text: (options ? options.defaultText || "" : ""),
                        bottomText: (options ? options.bottomText || "" : ""),
                    },
                },
                lock: false,
                rejectOnCancel: true,
            }).whenClosed((value) => {
                resolve(value.output)
            }).catch(() => {
                reject()
            })
        })
    }

    file(title, message, options, lock: boolean = false) {
        return new Promise((resolve, reject) => {
            this.service.open({
                viewModel: PLATFORM.moduleName("symfaurelia/dialogs/file-input"),
                view: PLATFORM.moduleName("symfaurelia/dialogs/file-input.html"),
                model: {
                    title: title,
                    message: message,
                    options: options,
                    cancellable: true,
                },
                lock: lock,
            }).whenClosed(response => {
                if (response.wasCancelled) {
                    reject()
                } else {
                    resolve(response.output)
                }
            })
        })
    }

}

function normalizeOptions(options: DialogOptions) {
    if (!options.autoClose) {
        options.autoClose = false
    }

    if (!options.translate) {
        options.translate = false
    }
    if (!options.icon) {
        switch (options.type) {
            case "question":
                options.icon = "question-circle"
                break
            case "info":
                options.icon = "exclamation-circle"
                break
        }
        options.translate = false
    }

    options.locked = options.locked || false

    if (!options.buttons) {
        options.buttons = []
    }

    for (let button of options.buttons) {
        if (!button.translate) {
            button.translate = false
        }

        if (!button.icon) {
            button.icon = null
        }

        if (!button.class) {
            button.class = "btn btn-secondary"
        }
    }

    return options
}

