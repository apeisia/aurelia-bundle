import {Dropdown} from "./dropdown";

export class DropdownCollection implements Map<string, Dropdown> {
    private map = new Map<string, Dropdown>();
    readonly [Symbol.toStringTag]: "Map";

    get(key: string): Dropdown {
        if (!this.has(key)) {
            this.set(key, new Dropdown());
        }

        return this.map.get(key);
    }

    get size(): number {
        return this.map.size;
    }

    [Symbol.iterator](): IterableIterator<[string, Dropdown]> {
        return this.map[Symbol.iterator]();
    }

    clear(): void {
        this.map.clear();
    }

    delete(key: string): boolean {
        return this.map.delete(key);
    }

    entries(): IterableIterator<[string, Dropdown]> {
        return this.map.entries();
    }

    forEach(callbackfn: (value: Dropdown, key: string, map: Map<string, Dropdown>) => void, thisArg?: any): void {
        this.map.forEach(callbackfn, thisArg);
    }

    has(key: string): boolean {
        return this.map.has(key);
    }

    keys(): IterableIterator<string> {
        return this.map.keys();
    }

    set(key: string, value: Dropdown): this {
        this.map.set(key, value);

        return this;
    }

    values(): IterableIterator<Dropdown> {
        return this.map.values();
    }
}
