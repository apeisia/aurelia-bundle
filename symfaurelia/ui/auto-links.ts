export class AutoLinks {
    makeLinks(str) {
        const anchorme = require("anchorme");

        if (!anchorme || !anchorme.default) {
            throw new Error("Missing dependency: anchorme");
        }

        if (!str) {
            return str;
        }

        return anchorme.default(this.escapeHTML(str), {
            truncate: [25, 5],
            attributes: [
                {
                    name: "target",
                    value: "_blank",
                }
            ]
        });
    }

    escapeHTML(str) {
        const textNode = document.createTextNode(str);
        const element = document.createElement("div");
        element.appendChild(textNode);

        return element.innerHTML;
    }
}

