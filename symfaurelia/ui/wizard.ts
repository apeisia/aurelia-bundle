export class Wizard {
    allowNavigateNext;
    currentStep = 0;
    steps = [];
    maxStep = 0; // the highest step that was visited

    enablePreviousStep = false;
    enableNextStep = true;
    enableSave = false; // is true when all wizard steps have been visited

    /**
     * @param steps Array of strings containing the names of the wizard steps
     * @param allowNavigateNext Should clicking on a new step work? (otherwise nextStep() is required)
     */
    constructor(steps: Array<String>, allowNavigateNext: boolean = true) {
        this.allowNavigateNext = allowNavigateNext;

        let i = 0;
        for (let stepName of steps) {
            this.steps.push({
                name: stepName,
                number: ++i,
                isActive: false,
                isDisabled: true,
                hasErrors: false,
                className: null,
            });
        }

        if (this.steps.length > 0) {
            this.steps[0].isActive = true;
            this.steps[0].isDisabled = false;
        }

        this.updateState();
    }

    /**
     * Enables visiting all steps. This is useful when using wizards for editing data.
     */
    enableAllSteps() {
        this.maxStep = this.steps.length - 1;
        this.updateState();
    }

    previousStep() {
        if (!this.enablePreviousStep) {
            return;
        }

        this.currentStep--;
        this.updateState();
    }

    nextStep() {
        if (!this.enableNextStep) {
            return;
        }

        this.currentStep++;
        this.updateState();
    }

    goToStep(step) {
        if (this.steps[step] && this.steps[step].isDisabled) {
            // don't go to disabled steps
            return;
        }

        this.currentStep = step;
        this.updateState();
    }
    
    setErrorsFromFormValidation(errors) {
        const checkForErrors = form => {
            if (form.children) {
                return checkForErrors(form.children);
            } else {
                for (let child of Object.values(form)) {
                    if ((<any>child).errors) {
                        return true;
                    }
                    // maybe refactor some day to only check one time for children
                    if ((<any>child).children) {
                        if(checkForErrors((<any>child).children)) {
                            return true;
                        }
                    }
                }

                return false;
            }
        };

        if (errors) {
            console.log("errors: ", JSON.parse(JSON.stringify(errors)));

            for (const stepName of Object.keys(errors.children)) {
                const stepChild = errors.children[stepName];
                this.steps[stepName.replace("step", "")].hasErrors = checkForErrors(stepChild.children);
            }
            console.log(this);

            this.updateState();
        }
    }

    private updateState() {
        this.maxStep = Math.max(this.currentStep, this.maxStep);
        this.enableNextStep = this.currentStep < this.steps.length - 1;
        this.enablePreviousStep = this.currentStep > 0;

        if (!this.enableSave && this.steps.length - 1 == this.currentStep) {
            this.enableSave = true;
        }

        for (let i = 0; i < this.steps.length; ++i) {
            const step = this.steps[i];

            step.isDisabled = i > this.maxStep + (this.allowNavigateNext ? 1 : 0);
            step.isActive = i == this.currentStep;

            let classes = [];

            if (step.isActive) {
                classes.push("active");
            }

            if (step.isDisabled) {
                classes.push("disabled");
            }

            if (step.hasErrors) {
                classes.push("has-errors");
            }

            step.className = classes.join(" ");
        }
    }
}
