import {debug} from "symfaurelia/log"

interface DropdownToggleOptions {
    applyContainerWidthToMenu?: boolean
}

/**
 * Helper for dropdowns and similar UI elements.
 *
 * Use `ref="dropdown.container"` on the outer element which you want to be clickable. The dropdown will not close
 * automatically for children of the container element. To make the button toggle the dropdown menu trigger the
 * `dropdown.toggle()` method.
 *
 * Use `ref="dropdown.menu"` on the menu that should open. If that element contains a `dropdown-menu` class the `show`
 * css class will be managed automatically (add when opening the dropdown, remove when closing). For elements that do
 * not contain a `dropdown-menu` you can use the externally exposed `isOpen` property of the `Dropdown` instance.
 *
 * @version 2.0
 */
export class Dropdown {
    isOpen = false
    container: HTMLElement
    menu: HTMLElement

    _boundClickListener
    _boundScrollListener
    _enableDebug = false

    constructor(button?, menu?) {
        // required as we always need the same function object for removeEventListener()
        this._boundClickListener = event => this.bodyClicked(event)
        this._boundScrollListener = event => this.bodyScrolled(event)

        // the DOM element that can be clicked without closing the dropdown
        this.container = button

        // the DOM element that should be positioned when opening the menu
        this.menu = menu
    }

    toggle(options: DropdownToggleOptions) {
        if (options?.applyContainerWidthToMenu) {
            this.menu.style.width = this.container.getBoundingClientRect().width + 'px'
        }

        if (this.isOpen) {
            this.debug("[DROPDOWN] toggle - close")
            this.close()
        } else {
            this.debug("[DROPDOWN] toggle - open")
            this.open()
        }

        return false
    }

    open() {
        this.debug("[DROPDOWN] opening")
        this.isOpen = true

        if (this.menu) {
            // a menu is set, move it to the right position

            // force show the menu for a short moment to get the size for correct horizontal positioning
            this.menu.style.display = "block"
            setTimeout(() => this.positionMenu(), 0) // execute after rendering

            if (this.menu.classList.contains("dropdown-menu")) {
                this.menu.classList.add("show")
            }
        }

        document.addEventListener("click", this._boundClickListener)
        document.addEventListener("scroll", this._boundScrollListener)
    }

    private positionMenu() {
        let buttonRect = this.container.getBoundingClientRect()
        let menuRect = this.menu.getBoundingClientRect()
        this.menu.style.display = null

        // set the menu position
        this.menu.style.position = "fixed"
        this.menu.style.top = (buttonRect.top + buttonRect.height) + "px"

        if ((menuRect.width + buttonRect.left) >= document.body.clientWidth) {
            // menu hits the right screen edge, position it to the right
            this.menu.style.right = "0px"
            this.menu.style.left = "auto"
        } else {
            // position normally at button level
            this.menu.style.left = buttonRect.left + "px"
            this.menu.style.right = "auto"
        }
        if (menuRect.height + menuRect.top > document.body.getBoundingClientRect().height) {
            document.body.style.height = (menuRect.height + menuRect.top) + "px"
        }
    }

    /**
     * Close the dropdown. If an event is given, the propagation of that event will be stopped.
     *
     * @param event
     */
    close(event?) {
        this.debug("[DROPDOWN] closing")
        this.isOpen = false

        document.removeEventListener("click", this._boundClickListener)
        document.removeEventListener("scroll", this._boundScrollListener)

        // if we moved away to a page that does not have the dropdown, menu and/or classList is undefined
        if (this.menu && this.menu.classList && this.menu.classList.contains("dropdown-menu")) {
            this.menu.classList.remove("show")
        }
        document.body.style.height = ''

        if (event) {
            event.stopPropagation()
        }
    }

    /**
     * event handler that is attached to the document to close the dropdown when clicked somewhere
     *
     * @param event
     */
    bodyClicked(event) {
        if (this.container === event.target || (this.container && this.container.contains(event.target))) {
            return
        }

        event.preventDefault()
        event.stopPropagation()

        this.debug("[DROPDOWN] closing because body was clicked, target:", event.target, "button:", this.container)
        this.close()
    }

    bodyScrolled(event) {
        this.positionMenu()
    }

    debug(...args) {
        if (this._enableDebug) {
            debug(...args)
        }
    }
}
