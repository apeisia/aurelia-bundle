export {deepCopy} from "./util/copy";
export {generateRandomName, generateRandomString} from "./util/random-string";
export {SubscriptionBag} from "./util/subscription-bag";
export {encodeQueryString, decodeQueryString} from "./util/encode-query-string";
export {filterObject, filterArrayObjects} from "./util/form";

export function sleep(milliseconds) {
    return new Promise(resolve => setTimeout(resolve, milliseconds));
}
