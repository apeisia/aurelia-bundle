import {autoinject} from "aurelia-dependency-injection";
import {AutoLinks} from "../ui/auto-links";

@autoinject()
export class AutoLinksValueConverter {
    constructor(private autoLinks: AutoLinks) {
    }

    toView(str) {
        return this.autoLinks.makeLinks(str);
    }
}
