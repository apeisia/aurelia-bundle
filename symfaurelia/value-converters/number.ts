export class NumberValueConverter {
    toView(n, decimalPlaces = 2) {
        return Number(n).toLocaleString('de', {maximumFractionDigits: decimalPlaces, minimumFractionDigits: decimalPlaces});
    }
}
