import NumberFormat = Intl.NumberFormat;

export class FileSizeValueConverter {

    toView(bytes, decimals = 2) {
        if (bytes === 0) return '0 Byte';

        const k = 1024;
        const dm = decimals < 0 ? 0 : decimals;
        const sizes = ['Byte', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];

        const i = Math.floor(Math.log(bytes) / Math.log(k));

        return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)).toLocaleString("de") + ' ' + sizes[i];
    }
}
