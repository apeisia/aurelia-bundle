const minutes = 60;
const hours = 60 * minutes;


function parseDateValue(value) {
    if (value instanceof Date) {
        return value;
    }

    return new Date(value);
}


export class DateValueConverter {
    toView(value) {
        if(value)
            return parseDateValue(value).toLocaleDateString(undefined, {year: 'numeric', month: '2-digit', day: '2-digit'});
        return '-';
    }
}

export class DatetimeValueConverter {
    toView(value) {
        if(value)
            return parseDateValue(value).toLocaleTimeString(undefined, {year: 'numeric', month: 'long', day: '2-digit', hour: '2-digit', minute: '2-digit'}).replace(",", "");
        return '-';
    }
}

export class ShortdatetimeValueConverter {
    toView(value) {
        if (value)
            return parseDateValue(value).toLocaleTimeString(undefined, {year: 'numeric', month: '2-digit', day: '2-digit', hour: '2-digit', minute: '2-digit'}).replace(",", "");
        return '-';
    }
}

export class TimeValueConverter {
    toView(value) {
        if (value)
            return parseDateValue(value).toLocaleTimeString(undefined, {hour: '2-digit', minute: '2-digit'}).replace(",", "");
        return '-';
    }
}

export class CustomDateValueConverter {
    toView(value, options) {
        if(value)
            return parseDateValue(value).toLocaleDateString(undefined, options);
        return '-';
    }
}

export class RelativeValueConverter {
    toView(value) {
        const now = new Date();
        const then = parseDateValue(value);
        const diff = Math.abs(now.getTime() - then.getTime()) / 1000;
        const past = now > then;

        if (diff >= 24 * hours) {
            // show absolute date time for timestamps more than 24 hours ago
            return new ShortdatetimeValueConverter().toView(then);
        }

        if (diff > 60 * minutes) {
            const h = Math.floor(diff / hours);
            return (past ? "vor " : "in ") + h + " Stunde" + (h == 1 ? "" : "n");
        }

        if (diff > minutes) {
            const h = Math.floor(diff / minutes);
            return (past ? "vor " : "in ") + h + " Minute" + (h == 1 ? "" : "n");
        }

        return (past ? "gerade eben" : "gleich");
    }
}

export class TimediffValueConverter {
    toView(value) {
        if(value[0] && value[1]) {
            const d1 = parseDateValue(value[0]);
            const d2 = parseDateValue(value[1]);
            const diff = Math.abs(d1.getTime() - d2.getTime()) / 1000;

            let h: number|string = Math.floor(diff / hours);
            let m: number|string = Math.floor((diff - (h * hours)) / minutes);
            let s: number|string = Math.floor(diff - (h * hours) - (m * minutes));

            if (h < 10) h = "0"+h;
            if (m < 10) m = "0"+m;
            if (s < 10) s = "0"+s;

            return h+':'+m+':'+s;
        }
        return '-';
    }
}
