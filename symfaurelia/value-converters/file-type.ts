export class FileTypeValueConverter {

    toView(t) {
        if (t === "application/pdf") {
            return "PDF"
        }

        if (t && t.indexOf("image/") === 0) {
            return "Bild"
        }

        return t
    }
}


export class FileTypeIconValueConverter {

    toView(t) {
        if (t === "application/pdf") {
            return "far fa-file-pdf"
        }

        if (t && t.indexOf("image/") === 0) {
            return "far fa-file-image"
        }

        return "far fa-file"
    }
}
