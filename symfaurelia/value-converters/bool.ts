export class BoolValueConverter {
    toView(value) {
        if(value)
            return "\u2714"; // utf8 check (✔)
        return "\u2716"; // utf8 X (✖)
    }
}
