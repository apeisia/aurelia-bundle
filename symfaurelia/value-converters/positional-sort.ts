export class PositionalSortValueConverter {
    toView(array, idProperty, order) {
        if (array === undefined) return;

        return array
            .slice()
            .sort((a, b) => {
                let idxA = order.indexOf(a[idProperty]);
                let idxB = order.indexOf(b[idProperty]);

                if (idxA > idxB) return 1;
                if (idxA < idxB) return -1;

                return 0;
            });
    }
}
