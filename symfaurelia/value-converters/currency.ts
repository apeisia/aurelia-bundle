
export class CurrencyValueConverter {
    formatter = new Intl.NumberFormat('de-DE', {
        style: 'currency',
        currency: 'EUR',
        minimumFractionDigits: 2
    });
    public static currencyFactor: number = 1

    toView(value, options) {
        if (value === null || isNaN(value)) {
            return "";
        }

        let formatter = this.formatter;
        if (options) {
            formatter = new Intl.NumberFormat('de-DE', Object.assign({}, {
                style: 'currency',
                currency: 'EUR',
                minimumFractionDigits: 2
            }, options));
        }

        return formatter.format(value / CurrencyValueConverter.currencyFactor);
    }
}
