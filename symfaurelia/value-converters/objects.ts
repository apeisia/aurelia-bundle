export class ObjectKeysValueConverter {
    toView(n) {
        if (!n) return n;

        return Object.keys(n);
    }
}

export class ObjectValuesValueConverter {
    toView(n) {
        if (!n) return n;

        return Object.values(n);
    }
}

export class ObjectEntriesValueConverter {
    toView(n) {
        if (!n) return n;

        console.log("object entries for: ", n, "=", Object.entries(n));

        return Object.entries(n);
    }
}

export class ObjectMapValueConverter {
    toView(n) {
        if (!n) return n;

        return new Map(Object.entries(n));
    }
}
