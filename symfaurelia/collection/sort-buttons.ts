import {bindable} from "aurelia-framework";

export class SortButtonsCustomElement {
    @bindable()
    item;
}
