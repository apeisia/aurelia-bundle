import {bindable} from "aurelia-framework";

export class CollectionCustomElement {
    @bindable()
    model;

    @bindable()
    view;

    @bindable()
    extra;
}
