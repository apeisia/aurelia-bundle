import {generateRandomName} from "symfaurelia/util/random-string"

export class SortableCollection {
    defaultCollapsed = true;
    newCollapsed = false;
    items = [];
    boundItems = [];
    extraFunctions = {};
    itemRemovedCallback;

    constructor(defaultCollapsed = true, newCollapsed = false, extraFunctions = {}) {
        this.defaultCollapsed = defaultCollapsed;
        this.newCollapsed = newCollapsed;
        this.extraFunctions = extraFunctions;
    }

    load(itemsToLoad) {
        this.items = [];
        this.boundItems = itemsToLoad;

        for (const itemContent of itemsToLoad) {
            this.items.push(this._createInternalItem(itemContent, this.defaultCollapsed));
        }

        this._updateItemsSortable();
    }

    append(itemContent, collapsed = this.newCollapsed) {
        const internalItem = this._createInternalItem(itemContent, collapsed);

        this.items.push(internalItem);
        this.boundItems.push(itemContent);

        // set canBeMovedUp / canBeMovedDown
        this._updateItemsSortable();

        return internalItem;
    }

    _createInternalItem(itemContent, collapsed = this.newCollapsed) {
        const internalItem = {
            'position': this.items.length,
            'collapsed': collapsed,
            'content': itemContent,
            'id': generateRandomName(),
            'toggle': null,
            'remove': null,
            'move': null,
            'extra': {},
        };

        internalItem.toggle = () => {
            internalItem.collapsed = !internalItem.collapsed;
        };

        internalItem.remove = () => {
            this.remove(internalItem);
        };

        internalItem.move = (direction) => {
            this.move(internalItem, direction);
        };

        // add extra functions to the item
        internalItem.extra = {};
        for (const functionName of Object.keys(this.extraFunctions)) {
            internalItem.extra[functionName] = this.extraFunctions[functionName](internalItem);
        }

        return internalItem;
    }

    remove(internalItem) {
        let idx = this.items.indexOf(internalItem);

        if (idx < 0) {
            return false;
        }

        // remove from internal items
        this.items.splice(idx, 1);
        this.items = this._recalculatePositions(this.items);

        // remove from bound items
        idx = this.boundItems.indexOf(internalItem.content);

        if (idx < 0) {
            return false;
        }

        this.boundItems.splice(idx, 1);

        if (this.itemRemovedCallback) {
            this.itemRemovedCallback(internalItem);
        }

        return true;
    }

    move(internalItem, direction) {
        if (this.canBeMoved(internalItem.position, direction)) {
            return;
        }

        const deltaPosition = direction === 'up' ? -1 : 1;

        // change position for the selected element and the element which we change positions with
        const changeWith = this.items[internalItem.position + deltaPosition];
        internalItem.position += deltaPosition;
        changeWith.position += deltaPosition * -1;

        // re-sort the whole array of fields by the position
        this._sortByPositions();

        // re-calculate the position properties
        this.items = this._recalculatePositions(this.items);

        return true;
    }

    canBeMoved(position, direction) {
        let disabledWhen = 0;

        if (direction === "down") {
            disabledWhen = this.items.length - 1;
        }

        return position === disabledWhen;
    }

    _recalculatePositions(internalItems) {
        let position = 0;

        for (const item of internalItems) {
            item.position = position++;

            this._updateItemSortable(item);
        }

        return internalItems;
    }

    _updateItemSortable(internalItem) {
        internalItem.canBeMovedUp = this.canBeMoved(internalItem.position, "up");
        internalItem.canBeMovedDown = this.canBeMoved(internalItem.position, "down");
    }

    _updateItemsSortable() {
        for (const internalItem of this.items) {
            this._updateItemSortable(internalItem);
        }
    }

    _sortByPositions() {
        this.items.sort((a, b) => {
            if (a.position < b.position) return -1;
            if (a.position > b.position) return 1;
            return 0;
        });

        this.boundItems.splice(0, this.boundItems.length);

        for (const internalItem of this.items) {
            this.boundItems.push(internalItem.content);
        }
    }
}
