<?php

namespace Apeisia\AureliaBundle\Twig;

use Symfony\Component\Form\FormView;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class AureliaFormExtension extends AbstractExtension
{
    public function getFunctions()
    {
        return [
            new TwigFunction('form_element_name', [$this, 'getFormElementName']),
            new TwigFunction('form_flat_element_name', [$this, 'getFormFlatElementName']),
        ];
    }

    public function getFormElementName(FormView $formView, $forErrorHandling = false)
    {
        $elementName = $formView->vars['name'];
        $formView    = $formView->parent;
        if ($formView && array_keys($formView->children) == ['date', 'time'] && $forErrorHandling) {
            $elementName = '';
        }

        while ($formView && $formView->parent !== null) {
            if (is_numeric($elementName))
                $add = '.__i' . $elementName; // set non-numeric magick name. this is processed in symfony-edit.js
            else if($elementName)
                $add = ($forErrorHandling ? '.children.' : '.') . $elementName;
            else
                $add = '';
            $elementName = $formView->vars['name'] . $add;
            $formView    = $formView->parent;

        }

        return $elementName;
    }

    public function getFormFlatElementName(FormView $formView)
    {
        // get something in the form of a[b][c] instead of a.b.c

        $path = [];

        while ($formView) {
            $path[] = $formView->vars['name'];
            $formView = $formView->parent;
        }

        // remove the form name
        array_pop($path);

        $path = array_reverse($path);
        $first = array_shift($path);

        return $first . (count($path) > 0 ? '[' . implode('][', $path) . ']' : '');
    }
}
