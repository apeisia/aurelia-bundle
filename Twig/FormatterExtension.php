<?php
namespace Apeisia\AureliaBundle\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class FormatterExtension extends AbstractExtension
{
    public function getFilters()
    {
        return [
            new TwigFunction('lpad', [$this, 'lpad']),
            new TwigFunction('rpad', [$this, 'rpad']),
        ];
    }

    public function lpad($str, $len, $fill = ' ', $truncate = false)
    {
        if ($truncate) {
            if (mb_strlen($str) > $len) {
                $str = substr($str, 0, $len);
            }
        }

        $pad = $len - mb_strlen($str);

        if ($pad > 0) {
            $str = str_repeat($fill, $pad) . $str;
        }

        return $str;
    }

    public function rpad($str, $len, $fill = ' ', $truncate = false)
    {
        if ($truncate) {
            if (mb_strlen($str) > $len) {
                $str = substr($str, 0, $len);
            }
        }

        $pad = $len - mb_strlen($str);

        if ($pad > 0) {
            $str = $str . str_repeat($fill, $pad);
        }

        return $str;
    }
}
