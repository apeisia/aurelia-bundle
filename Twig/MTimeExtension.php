<?php
namespace Apeisia\AureliaBundle\Twig;

use Symfony\Component\Form\FormView;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class MTimeExtension extends AbstractExtension
{
    private $rootDir;

    public function __construct($rootDir)
    {
        $this->rootDir = $rootDir;
    }

    public function getFunctions()
    {
        return [
            new TwigFunction('filemtime', [$this, 'getFileMtime']),
        ];
    }

    public function getFileMtime(string $file)
    {
        return filemtime($this->rootDir.'/'.$file);
    }
}
