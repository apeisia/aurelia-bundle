<?php

namespace Apeisia\AureliaBundle\Service;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Controller\ControllerResolverInterface;

class ControllerService
{
    private $controllerResolver;
    
    public function __construct(ControllerResolverInterface $controllerResolver)
    {
        $this->controllerResolver = $controllerResolver;
    }
    
    public function getVariablesFromController(string $bundle, string $controller, string $action, Request $request = null): array
    {
        $firstChar = $controller[0];
        if (strtolower($firstChar) == $firstChar) {
            throw new \InvalidArgumentException("@{$bundle}.{$controller}.{$action}: Controller name must start with a "
                . "uppercase character. The name will be transformed from CamelCase to under_score to find the view directory");
        }

        $action      = str_replace('.', '_', $action);
        $callable    = null;
        try {
            $callable = $this->resolveController($bundle, $controller, $action);
        } catch (\InvalidArgumentException $e) {
            // bc support: snake case AuVars
            $action = strtolower(preg_replace('/(?<!^)[A-Z]/', '_$0', $action));
            try {
                $callable = $this->resolveController($bundle, $controller, $action);
            } catch (\InvalidArgumentException $e) {
                return [];
            }
        }
        return $callable($request);
    }

    private function resolveController($bundle, $controller, $action) {

        $fakeRequest = new Request([], [], [
            '_controller' => "{$bundle}Bundle\\Controller\\{$controller}Controller::{$action}AuVars",
        ]);

        return $this->controllerResolver->getController($fakeRequest);
    }
}
