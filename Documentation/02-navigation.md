# Navigation
Aurelia only offers a very basic, flat menu generation using its router. Therefore we introduce a new class for
menu generation: the `Menu`. Just like the router it is configured via Symfony, more specifically the MenuRouter as
documented in [01-routing.md](01-routing.md). It also gets its configuration via REST.

## Configuration format
```js
let config = [
  // a menu item
  {"label": "Dashboard", "route": "dashboard"},
  // a submenu
  {"label": "Administration", "children":
    [
      // and its items
      {"label": "Users", "route": "users"},
      {"label": "Groups", "route": "groups"}
    ]}
];
```
