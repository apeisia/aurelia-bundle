# Routing
Allows you to configure the aurelia router AND menu within Symfony &rarr; MenuRouter.

## Menu structure (example):
* Left Menu *
    * Dashboard
    * Administration **
        * Users
        * Groups
* Top Menu *
    * User Menu **
        * My account
        * Change password
        * Logout

*) Not rendered, structural elements that can be dereferenced by a menu-unique name.

**) Parent menus without a route, just for rendering a menu

## Router configuration
The routes for the menu above need to be flattened and filtered like this:
* Dashboard
* Users
* Groups
* My account
* Change password
* Logout

Resulting in a json object to configure aurelia's router like this:
```json
[
  { "route": "", "name": "dashboard" /* ... */ },
  { "route": "users", "name": "users" /* ... */ }
]
```

## Menu configuration
See [02-navigation.md](02-navigation.md).

## PHP Example
To build the menu structure from above in PHP use:
```php
$root = new MenuRouter('root');
$leftMenu = new MenuRouter('left');
$topMenu = new MenuRouter('top');
$root->addChild($leftMenu);
$root->addChild($topMenu);

$leftMenu->addChild(new MenuRoute('dashboard', 'Dashboard', '/', '@Vwx.Dashboard.index'));
$adminMenu = new MenuRouter('admin', 'Administration');
$leftMenu->addChld($adminMenu);
$adminMenu->addChild(new MenuRoute('users', 'Users', '/user', '@Vwx.Users.index'));
// ...
```
