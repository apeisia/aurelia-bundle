# Changelog and migration guide Symfaurelia v2

* new branch `v2`: change `@dev-master` version constraint in `composer.json` to `@dev-v2`.

* `base.tpl.html.twig`:
    * is renamed to `index.ejs.twig`. This is the default value as set in the configuration if not
        overwritten.
    * the output path is changed from `templates/base.ejs` to `var/symfaurelia/index.ejs` to reduce
        confusion and save a .gitignore entry.

* Aurelia files in bundles:
    * dropped `au_` prefix
    * are now located in `<Bundle>/Resources/aurelia` instead of `<Bundle>/Resources/views`.
    * skeleton generators have been adjusted

* `au:skeleton` command
    * adjusted to output in the new format without prefix (see above)
    * added help text to the command
    * added `--force` (short `-f`) flag to overwrite existing files (previously files were always overwritten)

* made ApeisiaRestBundle obsolete
    * kernel exception listener is now part of our `KernelListener`
    * `fos_rest` config is now located in aurelia-bundle and needs to be imported from there

* changed event names to match new symfony convention
    * `aurelia.app_config` and `aurelia.router_config` are now invoked by their events class name

* List field names are now generated without quotes!

* changed default serializer group (crud) to "default" and added default error serializer
    group (which is "Default" by default).

## Todo
* Move commands to the new `AureliaBuildBundle`
* Fix `au:skeleton AppBundle News` (instead of `AppBundle AppBundle:News`)
* Change @-Notation to be directory based
